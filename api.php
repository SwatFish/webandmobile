<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Origin: http://localhost:3000");
header("Access-Control-Allow-Methods: GET, POST, PUT");
header("Access-Control-Allow-Headers: Content-Type, Authorization");

require_once __DIR__ . '/vendor/autoload.php';

use model\Status;
use model\Problem;
use model\Location;
use model\Technician;

$pdo = require_once './src/connector.php';
$router = new AltoRouter();

try {
    $containerBuilder = new DI\ContainerBuilder();
    $containerBuilder->useAutowiring(false);
    $containerBuilder->addDefinitions('src/phpDiConfig.php');
    $container = $containerBuilder->build();

    $locationController = $container->get('LocationController');
    $statusController = $container->get('StatusController');
    $problemController = $container->get('ProblemController');
    $technicianController = $container->get('TechnicianController');

//    $router->setBasePath('/api/');
    $router->setBasePath('/~hannecolaers/WP1/');

    $router->map('GET', 'locations',
        function () use ($locationController) {
            $locationController->handleGetLocations();
        }
    );

    $router->map('GET', 'locations/[i:location_id]/statuses',
        function ($location_id) use ($statusController) {
            $statusController->getStatusesByLocationId($location_id);
        }
    );

    $router->map('GET', 'locations/[i:location_id]/problems',
        function ($location_id) use ($problemController) {
            $problemController->getProblemByLocationId($location_id);
        }
    );

    $router->map('GET', 'problems',
        function () use ($problemController) {
            $problemController->getAllProblems();
        }
    );

    $router->map('GET', 'problem/[i:id]',
        function ($id) use ($problemController) {
            $problemController->getProblemById($id);
        }
    );

    $router->map('GET', 'problems/[:scheduledDate]',
        function ($scheduledDate) use ($problemController) {
            $problemController->getProblemsByScheduledDate($scheduledDate);
        }

    );

    $router->map('GET', 'technicians',
        function () use ($technicianController) {
            $technicianController->getAllTechnicians();
        }
    );

    $router->map('GET', 'technician/[i:id]',
        function ($id) use ($technicianController) {
            $technicianController->getTechnicianById($id);
        }
    );

    $router->map('POST', 'addStatus',
        function () use ($statusController) {
            $requestBody = file_get_contents('php://input');
            $jsonObject = json_decode($requestBody, true);
            $status = new Status(
                null,
                $jsonObject["location_id"],
                $jsonObject["status"],
                $jsonObject["date"]);
            $statusController->createStatus($status);
        }
    );

    $router->map('POST', 'addProblem',
        function () use ($problemController) {
            $requestBody = file_get_contents('php://input');
            $jsonObject = json_decode($requestBody, true);
            $problem = new Problem(
                null,
                $jsonObject["location_id"],
                $jsonObject["date"],
                $jsonObject["description"],
                $jsonObject["status"],
                $jsonObject["scheduled_date"],
                $jsonObject["picture"]);
            $problemController->createProblem($problem);
        }
    );

    $router->map('POST', 'addLocation',
        function () use ($locationController) {
            $requestBody = file_get_contents('php://input');
            $jsonObject = json_decode($requestBody, true);
            $location = new Location(
                null,
                $jsonObject["name"],
                $jsonObject["place"]);
            $locationController->createLocation($location);
        }
    );

    $router->map('POST', 'addTechnician',
        function () use ($technicianController) {
            $requestBody = file_get_contents('php://input');
            $jsonObject = json_decode($requestBody, true);
            $technician = new Technician(
                null,
                $jsonObject["first_name"],
                $jsonObject["last_name"]);
            $technicianController->createTechnician($technician);
        }
    );

    $router->map('PUT', 'editProblem/[i:id]',
        function () use ($problemController) {
            $content = trim(file_get_contents("php://input"));
            $decoded = json_decode($content, true);
            $problem = new Problem($decoded["id"], $decoded["location_id"], $decoded["date"], $decoded["description"], $decoded["status"], $decoded["scheduled_date"], $decoded["picture"]);

            $problemController->updateProblem($problem);
        }
    );

    $router->map('PUT', 'editTechnician/[i:id]',
        function () use ($technicianController) {
            $content = trim(file_get_contents("php://input"));
            $decoded = json_decode($content, true);
            $technician = new Technician($decoded["id"], $decoded["first_name"], $decoded["last_name"]);

            $technicianController->updateTechnician($technician);
        }
    );

    $router->map('PUT', 'editLocation/[i:id]',
        function () use ($locationController) {
            $content = trim(file_get_contents("php://input"));
            $decoded = json_decode($content, true);
            $location = new Location($decoded["id"], $decoded["name"], $decoded["place"]);
            $locationController->updateLocation($location);
        }
    );

    $match = $router->match();

    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']) && $_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'] == 'GET') {
            header("Access-Control-Allow-Origin: http://localhost:3000");
            header("Access-Control-Allow-Methods: GET, POST");
            header("Access-Control-Allow-Headers: Content-Type, Authorization");
        }
        exit;
    }

    if ($match && is_callable($match['target'])) {
        call_user_func_array($match['target'], $match['params']);
    } else {
        // no route was matched
        header($_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found');
    }

} catch (Exception $e) {
    echo($e->getMessage());
}
import React, {Component} from 'react';
import {
    BrowserRouter as Router,
    Route
} from 'react-router-dom';
import {LinkContainer, IndexLinkContainer} from 'react-router-bootstrap';
import OverviewPage from './overview/overview-page';
import HomePage from './home/home-page';
import PlanningPage from './planning/planning-page';
import TechnicianPage from './technicians/technician-page';
import {AppBar, Drawer, MenuItem} from 'material-ui';
import { connect } from 'react-redux';
import mapDispatchToPropsTitle from './common/title-dispatch-to-props';
import AddTechnicianPage from './technicians/technician-add-page';
import locationPage from './locations/location-add-page';
class Layout extends Component {
    constructor() {
        super();
        this.state = {drawerOpen: false};
    }

    toggleState = () => {
        const currentState = this.state.drawerOpen;
        this.setState({drawerOpen: !currentState});
    }

    render() {
        return (
            <Router>
                <div className="App">
                    <AppBar
                        title={this.props.title}
                        onLeftIconButtonTouchTap={this.toggleState}
                    />
                    <Drawer open={this.state.drawerOpen}>
                        <IndexLinkContainer to="/">
                            <MenuItem onClick={this.toggleState}>Home</MenuItem>
                        </IndexLinkContainer>
                        <LinkContainer to="/overzicht">
                            <MenuItem onClick={this.toggleState}>Overzicht</MenuItem>
                        </LinkContainer>
                        <LinkContainer to="/planning">
                            <MenuItem onClick={this.toggleState}>Planning</MenuItem>
                        </LinkContainer>
                        <LinkContainer to="/technicians">
                            <MenuItem onClick={this.toggleState}>Techniekers</MenuItem>
                        </LinkContainer>
                        <LinkContainer to="/locations">
                            <MenuItem onClick={this.toggleState}>Locations</MenuItem>
                        </LinkContainer>
                    </Drawer>
                    <Route exact={true} path="/" component={HomePage}/>
                    <Route path="/overzicht" component={OverviewPage}/>
                    <Route path="/planning" component={PlanningPage}/>
                    <Route exact={true} path="/technicians" component={TechnicianPage}/>
                    <Route path="/technicians/add" component={AddTechnicianPage}/>
                    <Route path="/locations" component={locationPage}/>
                </div>
            </Router>
        )
    }

    componentDidMount() {
        this.props.setTitle('Home');
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        title: state.title.title,
    }
}

export default connect(mapStateToProps, mapDispatchToPropsTitle)(Layout)


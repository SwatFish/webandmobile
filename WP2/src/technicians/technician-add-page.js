import React, { Component } from 'react';
import { connect } from "react-redux";
import mapDispatchToPropsTitle from '../common/title-dispatch-to-props';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import HttpService from '../common/http-service';
import { Link } from 'react-router-dom';
import moment from 'moment';
import {Grid, Row, Col} from 'react-bootstrap';


class TechnicianAddPage extends Component {
    constructor() {
        super();
        this.state = { showMessage: false };
    }

    render() {
        const message = (
            <div>
                <span>Technieker toegevoegd, klik <Link to='/technicians'>hier</Link> om terug te gaan.</span>
            </div>
        );
        return (
            <Grid id="main" fluid>
                <Row className="show-grid, lightBackground">
                    <div>
                        
                        <form onSubmit={this.save}>
                            <TextField hintText="first name" name="first_name" type="text" />
                            <TextField hintText="last name" name="last_name" type="text" />
                            <FlatButton label="Technieker toevoegen" primary="true" type="submit" />
                        </form>
                        {this.state.showMessage ? message : null}
                    </div>
                </Row>
            </Grid>
        );
    }
    save = (ev) => {
        ev.preventDefault();
        const first_name = ev.target['first_name'].value;
        const last_name = ev.target['last_name'].value;
    
        HttpService.addTechnician(first_name, last_name).then(() => {
            this.setState({ showMessage: true });
        });
    }
    componentDidMount() {
        this.props.setTitle('Technieker toevoegen');
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        ...mapDispatchToPropsTitle(dispatch, ownProps),
        addEntry: (entry) => {
            dispatch({ type: 'ADD_TECHNICIAN', payload: entry });
        }
    }
}

export default connect(undefined, mapDispatchToProps)(TechnicianAddPage)

import React, {Component} from 'react';
import HttpService from '../common/http-service';
import TechniciansList from './technicians-list';
import {Grid, Row, Col} from 'react-bootstrap';
import {connect} from 'react-redux';
import * as technicianActions from '../actions/technician-actions';
import mapDispatchToPropsTitle from '../common/title-dispatch-to-props';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import { Link } from 'react-router-dom';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';

class TechnicianPage extends Component {
    constructor() {
        super();
        this.state = {
            selectedTechnicianFirstName: '',
            selectedTechnicianLastName: '',
            showMessage: false
        };
    
        this.getTechnicians = this.getTechnicians.bind(this);
    }

    componentWillMount() {
            HttpService.getTechnicians()
                .then(fetchedTechnicians => this.props.setTechnicians(fetchedTechnicians));
    }

    getTechnicians(row) {
        const selectedTechnician = this.props.technicians[row];
        this.setState({selectedTechnicianId: selectedTechnician.id})
        this.setState({selectedTechnicianFirstName: selectedTechnician.first_name});
        this.setState({selectedTechnicianLastName: selectedTechnician.last_name});
    }

    render() {
        const message = (
            <div>
                <span>Technieker aangepast.</span>
            </div>
        );
        return (
            <Grid id="main" fluid>
                <Row className="show-grid, lightBackground">
                    <Col xs={12} md={6}>
                        <TechniciansList technicians={this.props.technicians}
                                       onRowClick={this.getTechnicians}/>
                    </Col>
                    <Col xs={12} md={6}>
                        <div >
                        <h3> Aanpassen van de geselecteerde Technieker. </h3>
                        <div>
                        <form onSubmit={this.update}>
                            <input value={this.state.selectedTechnicianId} name="id" type="hidden"/>
                            <TextField value={this.state.selectedTechnicianFirstName}  name="first_name" type="text" disabled='true'/>
                            <TextField value={this.state.selectedTechnicianLastName} name="last_name" type="text" disabled='true'/>
                            <TextField hintText="new firstname" name="new_first_name" type="text"/>
                            <TextField hintText="new lastname" name="new_last_name" type="text"/>
                            <FlatButton label="Technieker aanpassen" primary="true" type="submit" />
                        </form>
                        <h3 style={{color:"#111111"}}>{this.state.showMessage ? message : null}</h3>
                            </div>
                        </div>
                        <Link to="/technicians/add">
                            <FloatingActionButton style={{ position: 'fixed', right: '15px', bottom: '15px' }}>
                                <ContentAdd />
                            </FloatingActionButton>
                        </Link>
                
                    </Col>
                </Row>
            </Grid>
        );
    };

    update = (ev) => {
        ev.preventDefault();
        const first_name = ev.target['new_first_name'].value;
        const last_name = ev.target['new_last_name'].value;
        const id = ev.target['id'].value;

          HttpService.updateTechnician(id, first_name, last_name).then(() => {
            this.setState({ showMessage: true });
            this.state.selectedTechnicianFirstName = first_name;
            this.state.selectedTechnicianLastName = last_name;
            this.componentWillMount();
        });
        
    }

    componentDidMount() {
        this.props.setTitle('Technicians');
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        technicians: state.technicians.technicians
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        ...mapDispatchToPropsTitle(dispatch, ownProps),
        setTechnicians: (technicians) => {
            dispatch(technicianActions.setTechnicians(technicians));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(TechnicianPage);


import * as types from './action-types';

export function setTitle(title){
    return{ type: types.SET_TITLE, title };
}
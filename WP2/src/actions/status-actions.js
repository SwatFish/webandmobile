import * as types from './action-types';

export function setStatuses(statuses){
    return{ type: types.SET_STATUSES, statuses };
}

export function addStatus(status){
    return{ type: types.ADD_STATUS, status };
}


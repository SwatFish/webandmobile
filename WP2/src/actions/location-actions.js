import * as types from './action-types';

export function setLocations(locations){
    return{ type: types.SET_LOCATIONS, locations };
}
import * as types from './action-types';

export function setProblemsOfLocation(problems){
    return{ type: types.SET_PROBLEMS_OF_LOCATION, problems };
}

export function setAllProblems(problems){
    return{ type: types.SET_ALL_PROBLEMS, problems};
}

export function addProblem(problem){
    return{ type: types.ADD_PROBLEM, problem };
}

export function setProblemsOfScheduledDate(problems){
    return { type: types.SET_PROBLEMS_WITH_REQUESTED_SCHEDULED_DATE, problems};
}
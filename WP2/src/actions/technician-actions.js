import * as types from './action-types';

export function setTechnicians(technicians){
    return{ type: types.SET_TECHNICIANS, technicians };
}

// export function setTechnician(technician) {
//     return{ type: types.SET_TECHNICIAN, technician};
// }

export function addTechnician(technician){
    return{ type: types.ADD_TECHNICIAN, technician };
}
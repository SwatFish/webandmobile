import React, {Component} from 'react';
import {Grid, Row, Col} from 'react-bootstrap';
import 'react-infinite-calendar/styles.css';
import Calendar from './calendar';
import moment, {format} from "moment";
import 'moment/locale/nl-be';
import HttpService from '../common/http-service';
import {connect} from 'react-redux';
import * as problemActions from '../actions/problem-actions';
import * as locationActions from '../actions/location-actions';
import mapDispatchToPropsTitle from '../common/title-dispatch-to-props';
import DayPlanning from './planning';
import {Tabs, Tab} from 'material-ui';
import {teal500, teal50} from 'material-ui/styles/colors';
import MakePlanning from './make-planning';


class PlanningPage extends Component {

    constructor() {
        super();
        moment.locale('nl');
        this.state = {selectedDate: moment().format('YYYY-MM-DD')};
        this.handleSelectDate = this.handleSelectDate.bind(this);
        this.getProblemsOfScheduledDate = this.getProblemsOfScheduledDate.bind(this);
    }

    componentWillMount() {
        HttpService.getLocations()
            .then(fetchedLocations => this.props.setLocations(fetchedLocations));
        this.getProblemsOfScheduledDate();
        HttpService.getAllProblems()
            .then(fetchedProblems => this.props.setAllProblems(fetchedProblems));

    }

    getProblemsOfScheduledDate() {
        HttpService.getProblemsOfScheduledDate(this.state.selectedDate)
            .then((fetchedProblems) => {
                for (let problem of fetchedProblems) {
                    problem.status = problem.status == 1;
                    console.log(problem.status);
                }
                this.props.setProblemsWithScheduledDate(fetchedProblems);
            });
    }

    handleSelectDate = (date) => {
        this.setState({selectedDate: moment(date).format('YYYY-MM-DD')});
        this.getProblemsOfScheduledDate();
    }

    render() {
        const $date = moment(this.state.selectedDate).format('dddd DD MMMM');
        return (
        <Tabs inkBarStyle={{background: teal500}}>
            <Tab label="Bekijk planning" style={{backgroundColor: teal50, color: teal500}}>
                <Grid id="main" fluid>
                    <div className="lightBackground">
                        <Row className="show-grid">
                            <Col xs={12} md={8} mdOffset={2}>
                                <Calendar onSelect={this.handleSelectDate}/>
                            </Col>
                        </Row>
                        <Row className="show-grid" style={{marginTop: '4%'}}>
                            <Col xs={12} md={4} style={{ float: 'left', textAlign: 'right'}}>
                                <h2>{$date}</h2>
                            </Col>
                            <Col xs={12} md={8} style={{borderLeft: '1px solid black'}} id="dayPlanning">
                                <DayPlanning/>
                            </Col>
                        </Row>
                    </div>
                </Grid>
            </Tab>
            <Tab label="Maak planning" style={{backgroundColor: teal50, color: teal500}}>
                <Grid id="main" fluid >
                    <div className="lightBackground">
                        <Row className="show-grid">
                            <Col xs={12} md={10} mdOffset={1}>
                                <h1 style={{textAlign: 'center'}}>Openstaande niet ingeplande problemen</h1>
                                <MakePlanning/>
                            </Col>
                        </Row>
                    </div>
                </Grid>
            </Tab>
        </Tabs>
        )
    }

    componentDidMount() {
        this.props.setTitle('Planning');
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        problemsOfLocation: state.problems.problemsOfLocation,
        problemsWithScheduledDate: state.problems.problemsWithScheduledDate,
        locations: state.locations.locations,
        problems: state.problems.allProblems
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        ...mapDispatchToPropsTitle(dispatch, ownProps),
        setProblemsWithScheduledDate: (problems) => {
            dispatch(problemActions.setProblemsOfScheduledDate(problems));
        },
        setLocations: (locations) => {
            dispatch(locationActions.setLocations(locations));
        },
        setAllProblems: (problems) => {
            dispatch(problemActions.setAllProblems(problems));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(PlanningPage);

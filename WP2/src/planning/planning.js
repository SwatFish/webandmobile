import React, {Component} from 'react';
import {Checkbox, Divider} from 'material-ui';
import {connect} from 'react-redux';
import {teal500} from 'material-ui/styles/colors';
import * as problemActions from '../actions/problem-actions';
import HttpService from '../common/http-service';

class DayPlanning extends Component {
    constructor(props) {
        super(props);
        this.getLocationName = this.getLocationName.bind(this);
        this.handleChecked = this.handleChecked.bind(this);
    }

    getLocationName(locationId) {
        if (this.props.locations) {
            const $location = this.props.locations.find(l => l.id === locationId);
            return $location.name + ' - ' + $location.place;
        }
    }

    handleChecked = (event, isInputChecked, id) => {
        const status = isInputChecked ? 1 : 0;
        const newProblems = this.props.problemsWithScheduledDate.map((problem) => {
            if (id === problem.id){
                const updatedProblem = {...problem, status: status};
                HttpService.updateProblem(updatedProblem);
                return updatedProblem;
            } else{
                return problem;
            }
        });
        this.props.setProblemsWithScheduledDate(newProblems);

    }

    render() {
        const problems = this.props.problemsWithScheduledDate ? this.props.problemsWithScheduledDate : [];
        return (
            <div className="xsCenterText">
                { problems === [] ? 'Er zijn nog geen taken gepland voor deze dag' : ''}
                { problems.map(p =>
                    <div key={p.id}>
                        <b style={{
                            color: teal500,
                            textTransform: 'uppercase'
                        }}>{this.getLocationName(p.location_id)}</b>
                        <p><b>{p.date}</b> {p.description}</p>
                        <Checkbox
                            label="Afgehandeld"
                            checked={p.status == 1}
                            onCheck={(event, isInputChecked) => this.handleChecked(event, isInputChecked, p.id)}
                        />
                        <Divider  style={{marginTop: '2%', marginBottom: '2%'}}/>
                    </div>
                )}
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        setProblemsWithScheduledDate: (problems) => {
            dispatch(problemActions.setProblemsOfScheduledDate(problems));
        },
    }
};

const mapStateToProps = (state, ownProps) => {
    return {
        problemsWithScheduledDate: state.problems.problemsWithScheduledDate,
        locations: state.locations.locations
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(DayPlanning);
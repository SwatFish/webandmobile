import React, {Component} from 'react';
import InfiniteCalendar from 'react-infinite-calendar';
import {teal500} from 'material-ui/styles/colors';
import {grey800} from 'material-ui/styles/colors';

class Calendar extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <InfiniteCalendar
                displayOptions={{
                    showOverlay: false,
                    showHeader: false
                }}
                locale={{
                    locale: require('date-fns/locale/nl'),
                    headerFormat: 'dddd, D MMM',
                    weekdays: ["Zo", "Ma", "Di", "Woe", "Do", "Vrij", "Za", ],
                    weekStartsOn: 1,
                    todayLabel: {
                        long: 'Vandaag',
                        short: 'Nu.'
                    }
                }}
                theme={{
                    selectionColor: teal500,
                    weekdayColor: grey800,
                    headerColor: grey800,

                }}
                width='100%'
                height={250}
                selectedDate={new Date()}
                onSelect={(date) => this.props.onSelect(date)}
            />
        )
    }
}

export default Calendar
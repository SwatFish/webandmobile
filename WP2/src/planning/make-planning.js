import React, {Component} from 'react';
import {RaisedButton, Divider, DatePicker} from 'material-ui';
import {connect} from 'react-redux';
import {teal500, grey500} from 'material-ui/styles/colors';
import HttpService from '../common/http-service';
import moment, {format} from "moment";
import * as problemActions from '../actions/problem-actions';

class MakePlanning extends Component {
    constructor(props) {
        super(props);
        this.getLocationName = this.getLocationName.bind(this);
        this.updateProblem = this.updateProblem.bind(this);
        this.updateProblemsList = this.updateProblemsList.bind(this);
    }

    getLocationName(locationId) {
        if (this.props.locations) {
            const $location = this.props.locations.find(l => l.id === locationId);
            return $location.name + ' - ' + $location.place;
        }
    }

    updateProblemsList = (id, scheduled_date) => {
        const newProblems = this.props.problems.map((problem) => {
            if (id === problem.id) {
                return {...problem, scheduled_date: scheduled_date};
            } else {
                return problem;
            }
        });
        this.props.setAllProblems(newProblems);
    }

    updateProblem = (e, problem) => {
        e.preventDefault();
        const date = moment(e.target["date"].value).format('YYYY-MM-DD');
        problem.scheduled_date = date;
        HttpService.updateProblem(problem)
            .then(this.updateProblemsList(problem.id, date));
    };

    render() {
        const problems = this.props.problems ? this.props.problems : [];
        return (
            <div className="xsCenterText">
                { problems.filter(problem => problem.scheduled_date === null && problem.status === '0').map((problem, index) =>
                    <div key={problem.id}>
                        <p style={{
                            color: teal500,
                            textTransform: 'uppercase',
                            fontWeight: '700'
                        }}>{this.getLocationName(problem.location_id)}</p>
                        <p><b>{problem.date}</b> {problem.description}</p>
                        <form onSubmit={(e) => this.updateProblem(e, problem)}>
                            <label>Plan dit probleem in:</label>
                            <DatePicker hintText="Datum"
                                        style={{display: 'inline-block', marginLeft: '10px'}}
                                        name="date"
                            />
                            <br />
                            <RaisedButton label="Plan" primary={true} fullWidth={false} type="submit"/>
                        </form>
                        <Divider style={{marginTop: '20px', marginBottom: '20px', backgroundColor: grey500}}/>
                    </div>
                )}
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        setAllProblems: (problems) => {
            dispatch(problemActions.setAllProblems(problems));
        }
    }
};

const mapStateToProps = (state, ownProps) => {
    return {
        problems: state.problems.allProblems,
        locations: state.locations.locations
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MakePlanning);
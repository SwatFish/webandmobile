import axios from 'axios';

class HttpService {
    baseUrl = 'http://192.168.33.11/api';
    //baseUrl = 'http://192.168.241.138/~hannecolaers/WP1';

    getLocations() {
        return axios.get(`${this.baseUrl}/locations`)
            .then(r => r.data);
    }

    getStatusesOfLocation(location_id) {
        return axios.get(`${this.baseUrl}/locations/${location_id}/statuses`)
            .then(r => r.data);
    }

    getProblemsOfLocation(location_id) {
        return axios.get(`${this.baseUrl}/locations/${location_id}/problems`)
            .then(r => r.data);
    }

    getAllProblems() {
        return axios.get(`${this.baseUrl}/problems`)
            .then(r => r.data);
    }

    getProblemsOfScheduledDate(date) {
        return axios.get(`${this.baseUrl}/problems/${date}`)
            .then(r => r.data);
    }

    addStatus(location_id, status, date) {
        var headers =  {
            'Content-Type': 'application/json'
        }; 
        return axios.post(`${this.baseUrl}/addStatus`, { 
            location_id: location_id, status: status, date: date
        }, headers)
            .catch(function (error) {
            });
    }

    addProblem(location_id, date, description, technician_id, status, scheduled_date, picture) {
        var headers =  {
            'Content-Type': 'application/json'
        };
        return axios.post(`${this.baseUrl}/addProblem`, {
            location_id: location_id, date: date, description: description, status: status, scheduled_date: scheduled_date, picture: picture
        }, headers)
            .catch(function (error) {
            });
    }

    getTechnicians() {
        return axios.get(`${this.baseUrl}/technicians`)
            .then(r => r.data);
    }
    addLocation(name, place) {
        var headers =  {
            'Content-Type': 'application/json'
        };

        return axios.post(`${this.baseUrl}/addLocation`, {
                name: name,
                place: place},
            headers)
            .then(r => {
                return r.data
            })
            .catch(function (error) {
            });
    }

    addTechnician(first_name, last_name) {
        var headers =  {
            'Content-Type': 'application/json'
        }; 
        
        return axios.post(`${this.baseUrl}/addTechnician`, {first_name: first_name, last_name: last_name}, headers) 
        .then(r => {
            return r.data
        })
        .catch(function (error) {
        });
    }

    getTechnicianById(id) {
        return axios.get(`${this.baseUrl}/technician/${id}`)
            .then(r => r.data);
    }

    updateTechnician(id, first_name, last_name) {
        var headers =  {
            'Content-Type': 'application/json'
        }; 
        return axios.put(`${this.baseUrl}/editTechnician/${id}`, {
                "id": id,
                "first_name": first_name,
                "last_name": last_name
              }, headers)
        .then(r => r.data)
           .catch(function (error) {
            });
    }

    updateLocation(id, name, place) {
        var headers =  {
            'Content-Type': 'application/json'
        };
        return axios.put(`${this.baseUrl}/editLocation/${id}`, {
            "id": id,
            "name": name,
            "place": place
        }, headers)
            .then(r => r.data)
            .catch(function (error) {
            });
    }

    updateProblem(problem) {
        var headers =  {
            'Content-Type': 'application/json'
        };
        return axios.put(`${this.baseUrl}/editProblem/${problem.id}`, problem
        , headers).then(r => console.log( r.data))
           .catch(function (error) {
            });
    }
}

const httpService = new HttpService();

export default httpService;

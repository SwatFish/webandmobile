const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        setTitle: (title) => {
            dispatch({type: 'SET_TITLE', title: title});
        }
    }
}

export default mapDispatchToProps;
import { Line, Circle } from 'rc-progress';
import React, {Component} from 'react';

class rcProgress extends Component {
    constructor() {
        super();
        this.state = {
            percent: 10
        };
    }
    render() {
        const containerStyle = {
        width: '500px',
    };
        return (
            <div style={containerStyle}>
            Process :    <Line percent={this.state.percent} strokeWidth="1" strokeColor="#FF0000" />
            </div>
        );
    };

}

export default rcProgress


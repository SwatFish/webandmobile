import React, {Component} from 'react';
import HttpService from '../common/http-service';
import LocationsList from '../overview/locations-list';
import {Grid, Row, Col} from 'react-bootstrap';
import {connect} from 'react-redux';
import * as locationActions from '../actions/location-actions';
import mapDispatchToPropsTitle from '../common/title-dispatch-to-props';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';

class LocationPage extends Component {
    constructor() {
        super();
        this.state = {
            selectedLocationPlace: '',
            selectedLocationName: '',
        };

        this.getLocations = this.getLocations.bind(this);
    }

    componentWillMount() {
        HttpService.getLocations()
            .then(fetchedLocations => this.props.setLocations(fetchedLocations));
    }

    getLocations(row) {
        const selectedLocation = this.props.locations[row];
        this.setState({selectedLocationPlace: selectedLocation.place});
        this.setState({selectedLocationName: selectedLocation.name});
        this.setState({selectedLocationId: selectedLocation.id});
    }

    render() {
        return (
            <Grid id="main" fluid>
                <Row className="show-grid, lightBackground">
                    <Col xs={12} md={5}>
                        <LocationsList locations={this.props.locations}
                                         onRowClick={this.getLocations}/>
                    </Col>
                    <Col xs={12} md={7}>
                        <div >
                            <h3> Aanpassen van de geselecteerde Locatie. </h3>
                            <div>
                                <form onSubmit={this.saveLoc}>
                                    <input value={this.state.selectedLocationId} name="id" type="hidden"/>
                                    <TextField value={this.state.selectedLocationName} name="loc_name" type="text" disabled='true'/>
                                    <TextField value={this.state.selectedLocationPlace} name="loc_place" type="text" disabled='true'/>
                                    <TextField hintText="new location name" name="new_loc_name" type="text"/>
                                    <TextField hintText="new location place" name="new_loc_place" type="text"/>
                                    <FlatButton label="locatie aanpassen" primary="true" type="submit" />
                                </form>

                            </div>
                        </div>
                        <div>
                            <h1>Aanmaken nieuwe locatie</h1>
                            <form onSubmit={this.addLoc}>
                                <TextField hintText="Locatie naaam" name="loc_name" type="text" />
                                <TextField hintText="Locatie plaats" name="loc_place" type="text" />
                                <FlatButton label="Locatie toevoegen" primary="true" type="submit" />
                            </form>
                        </div>
                    </Col>
                </Row>
            </Grid>
        );
    };

    addLoc = (loc) => {
        loc.preventDefault();
        const name = loc.target['loc_name'].value;
        const place = loc.target['loc_place'].value;

        HttpService.addLocation(name, place).then(() => {
            this.setState({ showMessage: true });
            this.componentWillMount();
        });
    };

    componentDidMount() {
        this.props.setTitle('Technieker toevoegen');
    }

    saveLoc = (ev) => {
        ev.preventDefault();
        const name = ev.target['new_loc_name'].value;
        const place = ev.target['new_loc_place'].value;
        const id = ev.target['id'].value;

        HttpService.updateLocation(id, name, place).then(() => {
            this.componentWillMount();
        });
    };

}

const mapStateToProps = (state, ownProps) => {
    return {
        locations: state.locations.locations
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        ...mapDispatchToPropsTitle(dispatch, ownProps),
        setLocations: (locations) => {
            dispatch(locationActions.setLocations(locations));
        }
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(LocationPage);

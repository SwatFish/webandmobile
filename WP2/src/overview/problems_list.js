import React, {Component} from 'react';
import {Table, TableHeaderColumn, TableHeader, TableRow, TableBody, TableRowColumn} from 'material-ui';

class ProblemsList extends Component {
    render() {
        const problems = this.props.problems ? this.props.problems : [];
        return (
            <div>
                <Table>
                    <TableHeader displaySelectAll={false}
                                 adjustForCheckbox={false}
                                 enableSelectAll={false}>
                        <TableRow>
                            <TableHeaderColumn>Probleem</TableHeaderColumn>
                            <TableHeaderColumn>Is afgehandeld?</TableHeaderColumn>
                            <TableHeaderColumn>Datum</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody displayRowCheckbox={false}>
                        {problems.map((row, index) => (
                            <TableRow key={row.id}>
                                <TableRowColumn>{row.description}</TableRowColumn>
                                <TableRowColumn>{row.status === '0' ? 'neen' : 'ja' }</TableRowColumn>
                                <TableRowColumn>{row.date}</TableRowColumn>
                            </TableRow>
                        ))}
                        <TableRow>
                            <TableRowColumn>{problems.length === 0 ? 'Er zijn geen problemen voor deze locatie' : ''}</TableRowColumn>
                        </TableRow>
                    </TableBody>
                </Table>
            </div>
        );
    }

}

export default ProblemsList

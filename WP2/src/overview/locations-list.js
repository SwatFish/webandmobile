import React, {Component} from 'react';
import {Table, TableHeaderColumn, TableHeader, TableRow, TableBody, TableRowColumn} from 'material-ui';

class LocationsList extends Component {
    constructor(props) {
        super(props);
        this.state = {selected: ''};
        this.handleRowSelection = this.handleRowSelection.bind(this);
    }

    handleRowSelection = (selectedRow) => {
        this.setState({
            selected: selectedRow,
        });
        this.props.onRowClick(selectedRow);
    };

    isSelected = (index) => {
        return this.state.selected.indexOf(index) !== -1;
    };

    render() {
        const locations = this.props.locations ? this.props.locations : [];
        return (
            <div>
                <h3>Selecteer een locatie</h3>
                <Table selectable={true} onRowSelection={this.handleRowSelection}>
                    <TableHeader>
                        <TableRow>
                            <TableHeaderColumn>Naam</TableHeaderColumn>
                            <TableHeaderColumn>Plaats</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody
                        displayRowCheckbox={true}
                        deselectOnClickaway={false}
                        showRowHover={true}>
                        {locations.map((row, index) => (
                            <TableRow key={row.id} selected={this.isSelected(index)}>
                                <TableRowColumn>{row.name}</TableRowColumn>
                                <TableRowColumn>{row.place}</TableRowColumn>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </div>
        );
    }
}
;

export default LocationsList;
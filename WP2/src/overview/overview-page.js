import React, {Component} from 'react';
import HttpService from '../common/http-service';
import LocationsList from './locations-list';
import {Grid, Row, Col} from 'react-bootstrap';
import StatusesList from './statuses_list';
import ProblemsList from './problems_list';
import {connect} from 'react-redux';
import * as locationActions from '../actions/location-actions';
import * as problemActions from '../actions/problem-actions';
import * as statusActions from '../actions/status-actions';
import mapDispatchToPropsTitle from '../common/title-dispatch-to-props';

class OverviewPage extends Component {
    constructor() {
        super();
        this.state = {
            selectedLocationName: '',
            selectedLocationPlace: ''
        };

        this.getStatusesAndProblemsOfLocation = this.getStatusesAndProblemsOfLocation.bind(this);
    }

    componentWillMount() {
            HttpService.getLocations()
                .then(fetchedLocations => this.props.setLocations(fetchedLocations));
    }

    getStatusesAndProblemsOfLocation(row) {
        const selectedLocation = this.props.locations[row];
        this.setState({selectedLocationName: selectedLocation.name});
        this.setState({selectedLocationPlace: selectedLocation.place});

        HttpService.getStatusesOfLocation(selectedLocation.id)
            .then(fetchedStatuses => this.props.setStatuses(fetchedStatuses));

        HttpService.getProblemsOfLocation(selectedLocation.id)
            .then(fetchedProblems => this.props.setProblems(fetchedProblems));

    }

    render() {
        const locationString = "Locatie: " + this.state.selectedLocationName + " - " + this.state.selectedLocationPlace;
        return (
            <Grid id="main" fluid>
                <Row className="show-grid, lightBackground">
                    <Col xs={12} md={5}>
                        <LocationsList locations={this.props.locations}
                                       onRowClick={this.getStatusesAndProblemsOfLocation}/>
                    </Col>

                    <Col xs={12} md={7}>
                        <h3 >{ this.state.selectedLocationName === '' ? 'U heeft nog geen locatie geselecteerd' : locationString}</h3>
                        <div >
                            <StatusesList statuses={this.props.statuses}/>
                        </div>
                        <div style={{marginTop: '5%'}}>
                            <ProblemsList problems={ this.props.problems}/>
                        </div>
                    </Col>
                </Row>
            </Grid>
        );
    };
    componentDidMount() {
        this.props.setTitle('Overzicht');
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        locations: state.locations.locations,
        problems: state.problems.problemsOfLocation,
        statuses: state.statuses.statuses
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        ...mapDispatchToPropsTitle(dispatch, ownProps),
        setLocations: (locations) => {
            dispatch(locationActions.setLocations(locations));
        },
        setStatuses: (statuses) => {
            dispatch(statusActions.setStatuses(statuses));
        },
        setProblems: (problems) => {
            dispatch(problemActions.setProblemsOfLocation(problems));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(OverviewPage);

import React, {Component} from 'react';
import {Table, TableHeaderColumn, TableHeader, TableRow, TableBody, TableRowColumn} from 'material-ui';

class StatusesList extends Component {
    render() {
        const statuses = this.props.statuses ? this.props.statuses : [];
        return (
            <div>
                <Table>
                    <TableHeader displaySelectAll={false}
                                 adjustForCheckbox={false}
                                 enableSelectAll={false}>
                        <TableRow>
                            <TableHeaderColumn>Status</TableHeaderColumn>
                            <TableHeaderColumn>Datum</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody displayRowCheckbox={false}>
                        {statuses.map((row, index) => (
                            <TableRow key={row.id} >
                                <TableRowColumn>{row.status}</TableRowColumn>
                                <TableRowColumn>{row.date}</TableRowColumn>
                            </TableRow>
                        ))}
                        <TableRow>
                            <TableRowColumn>{statuses.length === 0 ? 'Er zijn geen statussen voor deze locatie' : ''}</TableRowColumn>
                        </TableRow>
                    </TableBody>
                </Table>
            </div>
        );
    }
}

export default StatusesList

import * as types from '../actions/action-types';

const techniciansReducer = (state = [], action) => {
    switch (action.type) {
        case types.SET_TECHNICIANS:
            return {...state, ...{technicians: action.technicians}};
        case types.ADD_TECHNICIAN:
            return {...state, ...{technicians: [...state.technicians, action.technician]}};
        default:
            return state;
    }
};

export default techniciansReducer;
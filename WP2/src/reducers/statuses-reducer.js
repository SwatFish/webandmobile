import * as types from '../actions/action-types';

const statusesReducer = (state = [], action) => {
    switch (action.type) {
        case types.SET_STATUSES:
            return {...state, ...{statuses: action.statuses}};
        case types.ADD_STATUS:
            return {...state, ...{statuses: [...state.statuses, action.status]}};
        default:
            return state;
    }
};

export default statusesReducer;
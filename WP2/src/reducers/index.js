import {combineReducers} from 'redux';
import locations from './locations-reducer';
import statuses from './statuses-reducer';
import problems from './problems-reducer';
import technicians from './technicians-reducer';
import title from './title-reducer';

const rootReducer = combineReducers({
    locations,
    statuses,
    problems,
    title,
    technicians
});

export default rootReducer;
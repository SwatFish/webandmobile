import * as types from '../actions/action-types';

const problemsReducer = (state = [], action) => {
    switch (action.type) {
        case types.SET_PROBLEMS_OF_LOCATION:
            return {...state, ...{problemsOfLocation: action.problems}};
        case types.SET_PROBLEMS_WITH_REQUESTED_SCHEDULED_DATE:
            return {...state, ...{problemsWithScheduledDate: action.problems}};
        case types.SET_ALL_PROBLEMS:
            return {...state, ...{allProblems: action.problems }}
        default:
            return state;
    }
};

export default problemsReducer;
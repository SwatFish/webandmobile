import * as types from '../actions/action-types';

const initialState = {
    title: '',
};

const titleReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.SET_TITLE:
            return {...state, ...{title: action.title}};
        default:
            return state;
    }
};

export default titleReducer;
import * as types from '../actions/action-types';

const locationsReducer = (state = [], action) => {
    switch (action.type) {
        case types.SET_LOCATIONS:
            return {...state, ...{locations: action.locations}};
        default:
            return state;
    }
};

export default locationsReducer;
import React, {Component} from 'react';
import './App.css';
import store from './common/store';
import {Provider} from 'react-redux';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {teal500} from 'material-ui/styles/colors';
import Layout from './layout';

const muiTheme = getMuiTheme({
    palette: {
        primary1Color: teal500,
        pickerHeaderColor: teal500,
    },
});

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <MuiThemeProvider muiTheme={muiTheme}>
                    <Layout />
                </MuiThemeProvider>
            </Provider>
        );
    }
}

export default App;

import React, {Component} from 'react';
import {Row, Col} from 'react-bootstrap';
import {connect} from 'react-redux';
import * as locationActions from "../actions/location-actions";
import HttpService from '../common/http-service';
import moment from 'moment';
import {Link} from 'react-router-dom';

import {SelectField, MenuItem, DatePicker, TextField, RadioButton, RadioButtonGroup, RaisedButton} from 'material-ui';
import ThumbDown from 'material-ui/svg-icons/action/thumb-down';
import ThumbUp from 'material-ui/svg-icons/action/thumb-up';
import ThumbUpDown from 'material-ui/svg-icons/action/thumbs-up-down';
import {deepOrange800} from 'material-ui/styles/colors';
import {amber900} from 'material-ui/styles/colors';
import {green900} from 'material-ui/styles/colors';

const styles = {
    radioButton: {
        marginTop: 30,
        float: 'left',
        width: '33%'
    },
    radioButtonIcon: {
        height: 80,
        width: 80
    },
    radioButtonLabel: {
        lineHeight: 6
    },
    label: {
        marginTop: 20,
        marginRight: 20,
    }

};

class Form extends Component {
    constructor() {
        super();
        this.state = {
            selectedLocation: '',
            showMessage: '',
        }
    }

    componentWillMount() {
        HttpService.getLocations()
            .then(fetchedLocations => this.props.setLocations(fetchedLocations));
    }

    render() {
        const locations = this.props.locations ? this.props.locations : [];

        const message = (
            <Row className="show-grid">
                <Col xs={12}>
                    <h4 style={{textAlign: 'center', color: 'green'}}>Uw mening is doorgegeven,<Link to="/overzicht"> klik om naar het overzicht te gaan</Link></h4>
                </Col>
            </Row>
        );

        return (
            <div>
                {this.state.showMessage ? message : null}
                <Row className="show-grid">
                    <Col xs={12} md={6} mdOffset={3}>
                        <form style={styles.form} onSubmit={this.save}>
                            <SelectField
                                floatingLabelText="Selecteer de locatie"
                                style={{width: '100%'}}
                                name="location"
                                value={this.state.selectedLocation}
                                onChange={(event, index, value) => this.setState({selectedLocation: value})}>
                                {locations.map(l => <MenuItem value={l.id} primaryText={ l.name + ' - ' + l.place }
                                                              key={l.id}/>)}
                            </SelectField>

                            <div style={{marginBottom: '20px', marginTop: '20px'}}>
                                <label style={styles.label}>Op welke datum was u op deze locatie?</label>
                                <DatePicker hintText="Datum"
                                            style={{display: 'inline-block'}}
                                            name="date"
                                />
                            </div>

                            <div style={{marginBottom: '20px'}}>
                                <label style={styles.label}>Hoe was uw ervaring op deze locatie?</label>
                                <RadioButtonGroup name="status">
                                    <RadioButton
                                        value="niet goed"
                                        label="Niet goed"
                                        checkedIcon={<ThumbDown style={{fill: deepOrange800}}/>}
                                        uncheckedIcon={<ThumbDown style={{fill: '#9b9ba3'}}/>}
                                        style={styles.radioButton}
                                        iconStyle={styles.radioButtonIcon}
                                        labelStyle={styles.radioButtonLabel}
                                        className="statusRadioButton"
                                    />
                                    <RadioButton
                                        value="middelmatig"
                                        label="Middelmatig"
                                        checkedIcon={<ThumbUpDown style={{fill: amber900}}/>}
                                        uncheckedIcon={<ThumbUpDown style={{fill: '#9b9ba3'}}/>}
                                        style={styles.radioButton}
                                        iconStyle={styles.radioButtonIcon}
                                        labelStyle={styles.radioButtonLabel}
                                        className="statusRadioButton"
                                    />
                                    <RadioButton
                                        value="goed"
                                        label="Goed!"
                                        checkedIcon={<ThumbUp style={{fill: green900}}/>}
                                        uncheckedIcon={<ThumbUp style={{fill: '#9b9ba3'}}/>}
                                        style={styles.radioButton}
                                        iconStyle={styles.radioButtonIcon}
                                        labelStyle={styles.radioButtonLabel}
                                        className="statusRadioButton"
                                    />
                                </RadioButtonGroup>
                            </div>
                            <div style={{marginBottom: '20px'}}>
                                <label style={styles.label}>Wilt u een probleem melden over deze locatie?</label>
                                <TextField
                                    hintText="Probleem:"
                                    multiLine={true}
                                    rows={2}
                                    rowsMax={5}
                                    style={{width: '100%', display: 'block'}}
                                    name="problem"
                                />
                            </div>
                            <RaisedButton label="Verzend" primary={true} type="submit" fullWidth={true}/>
                        </form>
                    </Col>
                </Row>

            </div>
        );
    }

    save = (e) => {
        e.preventDefault();
        const location_id = this.state.selectedLocation;
        const date = e.target["date"].value;
        const dateToSend = moment(date).format('YYYY-MM-DD');
        const status = e.target["status"].value;
        HttpService.addStatus(location_id, status, dateToSend)
            .then(() => {
                this.setState({showMessage: true});
            });
        const problem = e.target["problem"].value;
        if (problem) {
            HttpService.addProblem(location_id, date, problem, null, '0', null, null).then(() => {
                this.setState({showMessage: true});
            });
        }

    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        locations: state.locations.locations
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        setLocations: (locations) => {
            dispatch(locationActions.setLocations(locations));
        },
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Form);


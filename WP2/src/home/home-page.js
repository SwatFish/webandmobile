import React, {Component} from 'react';
import {Grid,} from 'react-bootstrap';
import StatusForm from './form';

class HomePage extends Component {

    render() {
        return (
            <Grid id="main" fluid>
                <div className="lightBackground">
                    <h1 style={{textAlign: 'center'}}>Laat ons weten wat u van een locatie vindt</h1>
                    <StatusForm />
                </div>
            </Grid>
        );
    }
}

export default HomePage;

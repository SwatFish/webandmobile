<?php
//./vendor/bin/simple-phpunit
namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class TechnicianControllerTest extends WebTestCase
{
    public function testAddtechnician()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/addTechnician');
    }

    public function testEdittechnician()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/editTechnician');

    }

    public function testRemovetechnician()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/removeTechnician');
    }

     /**
     * @dataProvider urlProvider
     */
    public function testPageIsSuccessful($url)
    {
        $client = self::createClient();
        $client->request('GET', $url);

        $this->assertTrue($client->getResponse()->isSuccessful());
    }

    public function urlProvider()
    {
        return array(
            //array('/admin/getTechnicians'),
            //array('/technicus/getMyProblems')
        );
    }

//    public function testSecuredHello()
//    {
//        $client = self::createClient(array(),
//            array(
//                'email' => 'joa.zeelmaekers@hotmail.com',
//                'password' => 'B7D-YmB-awp-c87',
//        ));
//
//        $crawler = $client->request('GET', '/admin/getTechnicians');
//
//        $this->assertSame(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
//    }

}

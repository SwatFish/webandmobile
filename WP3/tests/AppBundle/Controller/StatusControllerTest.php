<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class StatusControllerTest extends WebTestCase
{

    public function testProblemFormRouteIsSuccessful()
    {
        $client = self::createClient();
        $client->request('GET', "/problemForm");

        $this->assertTrue($client->getResponse()->isSuccessful());
    }

    public function testGetStatusesRouteIsSuccessful()
    {
        $client = self::createClient();
        $client->request('GET', "/getStatuses/1");

        $this->assertNotEquals($client->getResponse()->getContent(),null);
    }

    public function testGetAllStatusesRouteIsSuccessful() {
        $client = self::createClient();
        $client->request('GET', "/getAllStatuses");

        $this->assertTrue($client->getResponse()->isSuccessful());
    }

}

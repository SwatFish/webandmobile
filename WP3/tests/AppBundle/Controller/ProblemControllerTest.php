<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProblemControllerTest extends WebTestCase
{
    /**
     * @dataProvider urlProvider
     */
    public function testPageIsSuccessful($url)
    {
        $client = self::createClient();
        $client->request('GET', $url);
        $this->assertTrue($client->getResponse()->isSuccessful());
    }

    /**
     * @dataProvider urlProviderGetBys
     */
    public function testGetObjectIsSuccesful($url) {
        $client = self::createClient();
        $client->request('GET', "/getStatuses/1");

        $this->assertNotEquals($client->getResponse()->getContent(),null);
    }

    public function urlProvider()
    {
        return array(
            array('/problemForm'),
            array('/addImage'),
            array('/getAllProblems'),
            //array('/werkbeheerder/getAllProblemsNoTechnician')
        );
    }

    public function urlProviderGetBys()
    {
        return array(
            array('/getProblems/1'),
            array('/getProblemById/1')
        );
    }

}

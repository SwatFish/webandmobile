<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Problem
 *
 * @ORM\Table(name="problem", indexes={@ORM\Index(name="IDX_D7E7CCC8E6C5D496", columns={"technician_id"}), @ORM\Index(name="IDX_D7E7CCC864D218E", columns={"location_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProblemRepository")
 */
class Problem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="date", type="string", length=255, nullable=false)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=false)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint", nullable=false)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="picture",type="string",length=255,nullable=true)
     * @Assert\File(mimeTypes={"image/png"},maxSize="10M")
     */
    private $picture;

    /**
     * @var \Location
     *
     * @ORM\ManyToOne(targetEntity="Location")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="location_id", referencedColumnName="id")
     * })
     */
    private $location;

    /**
     * @var \Technician
     *
     * @ORM\ManyToOne(targetEntity="Technician")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="technician_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     * })
     */
    private $technician;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getScheduledDate()
    {
        return $this->scheduledDate;
    }

    /**
     * @param string $scheduledDate
     */
    public function setScheduledDate($scheduledDate)
    {
        $this->scheduledDate = $scheduledDate;
    }

    /**
     * @return mixed
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param mixed $picture
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }

    /**
     * @return \Location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param \Location $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    /**
     * @return \Technician
     */
    public function getTechnician()
    {
        return $this->technician;
    }

    /**
     * @param \Technician $technician
     */
    public function setTechnician($technician)
    {
        $this->technician = $technician;
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 29/10/2017
 * Time: 22:27
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class LocationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('place', TextType::class, array(
                'label' => 'Lokaal :    '))
            ->add('save', SubmitType::class, array(
                'label' => 'Zoek lokaal'));
    }
}

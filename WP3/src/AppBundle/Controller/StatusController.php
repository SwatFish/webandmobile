<?php

namespace AppBundle\Controller;

use AppBundle\Form\LocationType;
use AppBundle\Entity\Location;
use AppBundle\Entity\Problem;
use AppBundle\Entity\Status;
use interfaces\statusInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class StatusController extends Controller
{

    /**
     * @Route("/statusForm", name="statusForm")
     */
    public function newAction(Request $request)
    {
        return $this->render(
            'AppBundle:Status:status_form.html.twig',
            array(
                'last_username' => null,
                'error'         => null,
            )
        );
    }
    /**
     * @Route("/statusForm/validate", name="statusValidate")
     */

    public function validateStatusAction(Request $request)
    {
        $place = $request->request->get('_place');
        $location = $this->getDoctrine()->getRepository(Location::class)->getLocationByPlace($place);

        if ($location != null) {
            $id = $location->getId();
            $statuses = $this->getDoctrine()->getRepository(Status::class)->getStatusesByLocation($id);
            $paginator = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
                $statuses,
                $request->query->get('page', 1),
                7
            );
            return $this->render(
                'AppBundle:Status:get_statuses.html.twig',
                array("pagination" => $pagination)
            );
        } else {
            return $this->render(
                'AppBundle:Status:status_form.html.twig',
                array(
                    'error' => true,
                )
            );
        }
    }


    /**
     * @Route("/getStatuses/{location_id}")
     */
    public function getStatusesAction(Request $request, $location_id)
    {
        $statuses = $this->getDoctrine()
            ->getRepository(Status::class)
            ->getStatusesByLocation($location_id);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $statuses,
            $request->query->get('page', 1),
            7
        );

        return $this->render('AppBundle:Status:get_statuses.html.twig', array(
            'pagination'=>$pagination
        ));
    }
    /**
     * @Route("/getAllStatuses", name="getAllStatuses")
     */
    public function getAllStatuses(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $statuses = $em->getRepository('AppBundle:Status')->findAll();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $statuses,
            $request->query->get('page', 1),
            7
        );

        return $this->render(
            'AppBundle:Status:get_statuses.html.twig',
            array("pagination"=>$pagination)
        );
    }
}

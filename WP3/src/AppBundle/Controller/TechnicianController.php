<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Problem;
use AppBundle\Entity\Technician;
use AppBundle\Entity\User;
use interfaces\problemInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class TechnicianController extends Controller
{

    /**
     * @Route("/werkbeheerder/removeTechnician/{problem_id}", name="removeTechFromProblem")
     */
    public function removeTechnicianFromProblem($problem_id)
    {

        $problem = $this->getDoctrine()->getRepository(Problem::class)->getProblemById($problem_id);
        $problem->setTechnician(null);
        $em = $this->getDoctrine()->getManager();
        $em->persist($problem);
        $em->flush();
        return $this->redirectToRoute('getAllProblemsNoTech');
    }

    /**
     * @Route("/werkbeheerder/addTechnician/{problem_id}", name="addTechnicianToProblem")
     */
    public function addTechnicianAction($problem_id)
    {
        $problem = $this->getDoctrine()->getRepository(Problem::class)->getProblembyLoc($problem_id);
        $technicians = $this->getDoctrine()->getRepository(Technician::class)->findAll();
        return $this->render(
            'AppBundle:Technician:add_technician.html.twig',
            array("problems"=>$problem, 'technicians'=>$technicians)
        );
    }

    /**
     * @Route("/werkbeheerder/addTechnicianToProblem/{technician_id}/{problem_id}", name="addSelectedTechToProblem")
     */
    public function giveProblemToTechnicianAction($technician_id, $problem_id)
    {
        $problem = $this->getDoctrine()->getRepository(Problem::class)->getProblemById($problem_id);
        $technician = $this->getDoctrine()->getRepository(Technician::class)->getTechnicianById($technician_id);
        $em = $this->getDoctrine()->getManager();
        $problem->setTechnician($technician);
        $em->persist($problem);
        $em->flush();

        $technicianUser = $this->getDoctrine()->getRepository(User::class)->getUserTechnician($technician);
        $transport = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 587, 'tls')
            ->setUsername('webandmobile3@gmail.com')
            ->setPassword('webandmobile')
            ->setStreamOptions(array(
                'ssl' => array(
                    'allow_self_signed' => true,
                    'verify_peer' => false)));

        $transport->setLocalDomain('[127.0.0.1]');

        $message = \Swift_Message::newInstance()
            ->setSubject('Nieuw probleem aan u toegekend')
            ->setFrom(['webandmobile3@gmail.com' => 'Web and Mobile'])
            ->setTo($technicianUser->getEmail())
            ->setBody(
                $this->renderView(
                    'AppBundle:Technician:email.html.twig',
                    array('problem' => $problem)
                ),
                'text/html'
            );


        $mailer = \Swift_Mailer::newInstance($transport);
        $sentFlag = $mailer->send($message);
        echo "<script type='text/javascript'>alert('ok');</script>";
        return $this->redirectToRoute('getAllProblemsNoTech');
    }

    /**
     * @Route("/technicus/getMyProblemsSolved", name="getYourProblemsSolved")
     */
    public function getIssuesSolvedAction(Request $request)
    {

        $user = $this->getUser();
        $usertech = $user->getTechnician();
        $problem = $this->getDoctrine()->getRepository(Problem::class)
            ->getAllSolvedProblemsFromtech($usertech);
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $problem,
            $request->query->get('page', 1),
            7
        );
        return $this->render('AppBundle:Problem:show_problems_technician.html.twig', array(
            "pagination"=>$pagination
        ));
    }

    /**
     * @Route("/technicus/getMyProblems", name="getYourProblems")
     */
    public function getIssuesAction(Request $request)
    {

        $user = $this->getUser();
        $usertech = $user->getTechnician();
        $problem = $this->getDoctrine()->getRepository(Problem::class)
            ->getAllUnSolvedProblemsFromtech($usertech);
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $problem,
            $request->query->get('page', 1),
            7
        );
        return $this->render('AppBundle:Problem:show_problems_technician.html.twig', array(
            "pagination"=>$pagination
        ));
    }

    /**
     * @Route("/admin/getTechnicians", name="editTechs")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editTechnicianAction(Request $request)
    {
        $techs= $this->getDoctrine()->getRepository(Technician::class)->findAll();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $techs,
            $request->query->get('page', 1),
            7
        );
        return $this->render('AppBundle:Technician:show_all_technicians.html.twig', array(
            "pagination"=>$pagination
        ));
    }

    /**
     * @Route("/technician/addLoggedTechToProb/{problem_id}", name="addProblem_to_logged_in_tech")
     */
    public function removeTechnicianAction($problem_id)
    {
        $user = $this->getUser();
        $usertech = $user->getTechnician();
        $problem = $this->getDoctrine()->getRepository(Problem::class)->getProblemById($problem_id);
        $em = $this->getDoctrine()->getManager();
        $problem->setTechnician($usertech);
        $em->persist($problem);
        $em->flush();
        return $this->redirectToRoute("getYourProblems");
    }


    /**
     * @Route("/technician/clearProblem{problem_id}", name="setProblemToDone")
     */
    public function setProblemDone($problem_id)
    {
        $problem = $this->getDoctrine()->getRepository(Problem::class)->getProblemById($problem_id);
        $problem->setStatus(1);
        $em = $this->getDoctrine()->getManager();
        $em->persist($problem);
        $em->flush();
        return $this->redirectToRoute("getYourProblems");
    }
}

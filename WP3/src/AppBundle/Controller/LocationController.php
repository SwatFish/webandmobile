<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Location;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Repository;

class LocationController extends Controller
{
    /**
     * @Route("/getLocations", name="getLocs")
     */
    public function getLocationsAction()
    {
        $locaties = $this->getDoctrine()
            ->getRepository(Location::class)
            ->findAllLocations();

        return $this->render(
            'AppBundle:Location:get_locations.html.twig',
            array("locs"=>$locaties)
        );
    }
}

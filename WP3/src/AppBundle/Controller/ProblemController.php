<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Location;
use AppBundle\Entity\Problem;
use AppBundle\Entity\Technician;
use AppBundle\Form;
use AppBundle\Repository\LocationRepository;
use SebastianBergmann\CodeCoverage\Report\Text;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\StreamedResponse;

class ProblemController extends Controller
{

    /**
     * @Route("/problemForm", name="problemForm")
     */
    public function newAction(Request $request)
    {
        return $this->render(
            'AppBundle:Problem:problem_form.html.twig',
            array(
                'last_username' => null,
                'error'         => null,
            )
        );
    }

    /**
     * @Route("/validateProblem", name="validateLocation")
     */
    public function validateProblem(Request $request)
    {

        $place = $request->request->get('_place');
        $location = $this->getDoctrine()->getRepository(Location::class)->getLocationByPlace($place);


        if ($location != null) {
            $id = $location->getId();
            $problems = $this->getDoctrine()->getRepository(Problem::class)->getProblemsByLocation($id);
            $paginator = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
                $problems,
                $request->query->get('page', 1),
                7
            );
            if ($this->get('security.authorization_checker')->isGranted('ROLE_WERKBEHEERDER')) {
                return $this->render(
                    'AppBundle:Problem:show_problems_werkbeheerder.html.twig',
                    array("pagination" => $pagination)
                );
            } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_TECHNICUS')) {
                return $this->render(
                    'AppBundle:Problem:show_problems_technician.html.twig',
                    array("pagination" => $pagination)
                );
            } else {
                return $this->render(
                    'AppBundle:Problem:show_problems_anon.html.twig',
                    array("pagination" => $pagination)
                );
            }
        } else {
            return $this->render(
                'AppBundle:Problem:problem_form.html.twig',
                array(
                    'error'         => true,
                )
            );
        }
    }

    /**
     * @Route("/addImage/{problem_id}", name="addImage")
     */
    public function addImage(Request $request, $problem_id)
    {
        $problem = $this->getDoctrine()->getRepository(Problem::class)->find($problem_id);

        $form = $this->createFormBuilder($problem)
            ->add('picture', FileType::class, array('label' => 'Picture (PNG file)',
                'data_class' => null, 'required' => false))
            ->add('save', SubmitType::class, array('label' => 'Upload image'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $problem->getPicture();
            if ($file != null) {
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move(
                    $this->getParameter('image_directory'),
                    $fileName
                );
                $problem->setPicture($fileName);
                $em = $this->getDoctrine()->getManager();
                $em->persist($problem);
                $em->flush();
            }
            return $this->redirectToRoute("getAllProblems");
        }

        return $this->render('AppBundle:Problem:new.html.twig', array('form' => $form->createView()));
    }

    /**
     * @Route("/downloadImage/{problem_id}", name="downloadImage")
     */
    public function downloadPicture($problem_id) {
        $problem = $this->getDoctrine()->getRepository(Problem::class)->find($problem_id);

        $image = $problem->getPicture();
        $path = $this->getParameter('image_directory');
        $pathToFile = $path. '/' . $image;
        $response = new BinaryFileResponse($pathToFile);

        return $response;
    }


    /**
     * @Route("/getProblems/{place}", name="getProblemsByPlace")
     */
    public function getProblemsByLocAction(Request $request, $place)
    {
        $location = $this->getDoctrine()->getRepository(Location::class)->getLocationByPlace($place);

        if ($location != null) {
            $id = $location->getId();
            $problems = $this->getDoctrine()->getRepository(Problem::class)->getProblemsByLocation($id);

            $paginator = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
                $problems,
                $request->query->get('page', 1),
                7
            );
            if ($this->get('security.authorization_checker')->isGranted('ROLE_WERKBEHEERDER')) {
                return $this->render(
                    'AppBundle:Problem:show_problems_werkbeheerder.html.twig',
                    array("pagination" => $pagination)
                );
            } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_TECHNICUS')) {
                return $this->render(
                    'AppBundle:Problem:show_problems_technician.html.twig',
                    array("pagination" => $pagination)
                );
            } else {
                return $this->render(
                    'AppBundle:Problem:show_problems_anon.html.twig',
                    array("pagination" => $pagination)
                );
            }
        } else {
            return $this->render(
                'AppBundle:Problem:show_problems_anon.html.twig',
                array("pagination" => null)
            );
        }
    }

    /**
     * @Route("/getProblemById/{id}", name="getProblemById")
     */
    public function getProblemByIdAction($id)
    {
        $problem = $this->getDoctrine()
            ->getRepository(Problem::class)
            ->getProblemById($id);

        return $this->render(
            'AppBundle:Problem:show_problems_anon.html.twig',
            array("problems" => $problem)
        );
    }

    /**
     * @Route("/getAllProblems", name="getAllProblems")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getAllProblemsAction(Request $request)
    {
        $problems = $this->getDoctrine()
            ->getRepository(Problem::class)
            ->findAllProblems();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $problems,
            $request->query->get('page', 1),
            7
        );

        if ($this->get('security.authorization_checker')->isGranted('ROLE_TECHNICUS')) {
            return $this->render(
                'AppBundle:Problem:show_problems_technician.html.twig',
                array('pagination' => $pagination)
            );
        } else {
            return $this->render(
                'AppBundle:Problem:show_problems_anon.html.twig',
                array('pagination' => $pagination)
            );
        }
    }

    /**
     * @Route("/technicus/csv", name="csv")
     *
     */
    public function csvAction()
    {
        $user = $this->getUser();
        $usertech = $user->getTechnician();
        $problems = $this->getDoctrine()->getRepository(Problem::class)
            ->getAllUnSolvedProblemsFromtech($usertech);

        $response = new StreamedResponse();
        $response->setCallback(
            function () use ($problems) {
                $handle = fopen('php://output', 'r+');
                fputcsv($handle, array("Problem", "datum probleemmelding", "Plaats"), ';', '"');
                foreach ($problems as $row) {
                    $data = array($row->getDescription(), $row->getDate(), $row->getLocation()->getPlace());
                    fputcsv($handle, $data, ';', '"');
                }
                fclose($handle);
            }
        );
        $response->headers->set('Content-Type', 'application/force-download');
        $response->headers->set('Content-Disposition', 'attachment; filename="tasks.csv"');

        return $response;
    }
    /**
     * @Route("/werkbeheerder/getAllProblemsNoTechnician", name="getAllProblemsNoTech")
     */
    public function problemViewWithTechnicians(Request $request)
    {
        $problems = $this->getDoctrine()
            ->getRepository(Problem::class)
            ->getAllUnSolvedProblems();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $problems,
            $request->query->get('page', 1),
            7
        );

        return $this->render(
            'AppBundle:Problem:show_problems_werkbeheerder.html.twig',
            array('pagination' => $pagination)
        );
    }

    public function showAction($problem_id)
    {
        $em = $this->getDoctrine();
        $problem = $em->getRepository(Problem::class)->find($problem_id);
        if (!$problem) {
            throw $this->createNotFoundException('Unable to find problem entity.');
        }
        return $this->render('AppBundle:Problem:show.html.twig', array(
            'entity'      => $problem,

        ));
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 05/11/2017
 * Time: 19:45
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Problem;
use AppBundle\Entity\Technician;
use interfaces\problemInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class AdminController extends Controller
{
    /**
     * @Route("/admin/showAllTechs", name="showAllTechs")
     */
    public function showAllTechs(Request $request)
    {
        $technicians = $this->getDoctrine()->getRepository(Technician::class)->findAll();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $technicians,
            $request->query->get('page', 1),
            7
        );

        return $this->render(
            'AppBundle:Technician:show_all_technicians.html.twig',
            array('pagination'=>$pagination
            )
        );
    }

    /**
     * @Route("/admin/createTechnician", name="createTechnician")
     */
    public function createTechnicianAction(Request $request)
    {

        return $this->render(
            'AppBundle:Admin:create_technician.html.twig',
            array(
                'last_username' => null,
                'error'         => null,
            )
        );
    }

    /**
     * @Route("/admin/validateTech", name="validateTech")
     */
    public function validateTechnicianAction(Request $request)
    {
        $firstname = $request->request->get('_firstname');
        $lastname = $request->request->get('_lastname');


        if (!empty($firstname) && !empty($lastname)) {
            $tech = new Technician();
            $tech->setFirstName($firstname);
            $tech->setLastName($lastname);
            $em = $this->getDoctrine()->getManager();
            $em->persist($tech);
            $em->flush();
            return $this->render(
                'AppBundle:Admin:create_technician.html.twig',
                array(
                    'last_username' => null,
                    'error'         => "Successvol toegevoegd",
                )
            );
        } else {
            return $this->render(
                'AppBundle:Admin:create_technician.html.twig',
                array(
                    'last_username' => null,
                    'error'         => "U heeft niet alle velden ingevuld.",
                )
            );
        }
    }

    /**
     * @Route("/admin/removeTech/{technician_id}", name="removeTechFromDb")
     */
    public function removeTechnicianAction(Request $request, $technician_id)
    {
        $technician = $this->getDoctrine()->getRepository(Technician::class)->find($technician_id);
        //$problems = $this->getDoctrine()->getRepository(Problem::class)->findBy(array('technician'=>$technician));
        $em = $this->getDoctrine()->getManager();
        $em->remove($technician);
        $em->flush();
        return $this->redirectToRoute("showAllTechs");
    }


    /**
     * @Route("/admin/editTechnician/{technician_id}", name="editTechnician")
     */
    public function editTechnicianAction(Request $request, $technician_id)
    {
        $technician = $this->getDoctrine()->getRepository(Technician::class)->find($technician_id);
        $first_name = $technician->getFirstName();
        $last_name = $technician->getLastName();
        $id = $technician->getId();
        return $this->render(
            'AppBundle:Admin:edit_technician.html.twig',
            array(
                'id'=> $id,
                'firstname' => $first_name,
                'lastname'  => $last_name,
                'error'=> null
            )
        );
    }

    /**
     * @Route("/admin/saveTech/{technician_id}", name="saveNewChanges")
     */
    public function saveTechnicianAction(Request $request, $technician_id)
    {
        var_dump($technician_id);
        $tech = $this->getDoctrine()->getRepository(Technician::class)->find($technician_id);
        $firstname = $request->request->get('_firstname');
        $lastname = $request->request->get('_lastname');

        if (!empty($firstname) && !empty($lastname)) {
            $tech->setFirstName($firstname);
            $tech->setLastName($lastname);
            $em = $this->getDoctrine()->getManager();
            $em->persist($tech);
            $em->flush();
            return $this->redirectToRoute("showAllTechs");
        } else {
            return $this->render(
                'AppBundle:Admin:edit_technician.html.twig',
                array(
                    'id'=> $tech->getId(),
                    'firstname' => "Vul voornaam in",
                    'lastname'  => "Vul achternaam in",
                    'error'         => "U heeft niet alle velden ingevuld.",
                )
            );
        }
    }
}

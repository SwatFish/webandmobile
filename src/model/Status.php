<?php
/**
 * Created by PhpStorm.
 * User: 11502325
 * Date: 29/09/2017
 * Time: 10:26
 */

namespace model;

class Status implements \JsonSerializable

{
    private $id;
    private $location_id;
    private $status;
    private $date;

    /**
     * Status constructor.
     * @param $id
     * @param $location_id
     * @param $status
     * @param $date
     */
    public function __construct($id,$location_id, $status, $date)
    {
        $this->setId($id);
        $this->setLocationId($location_id);
        $this->setStatus($status);
        $this->setDate($date);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLocationId()
    {
        return $this->location_id;
    }

    /**
     * @param mixed $location_id
     */
    public function setLocationId($location_id)
    {
        $this->location_id = $location_id;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
<?php

namespace model;

class Location implements \JsonSerializable
{
    private $id;
    private $name;
    private $place;

    public function __construct($id, $name, $place)
    {
        $this->setId($id);
        $this->setName($name);
        $this->setPlace($place);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * @param mixed $place
     */
    public function setPlace($place)
    {
        $this->place = $place;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);

    }
}
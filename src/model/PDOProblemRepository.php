<?php
/**
 * Created by PhpStorm.
 * User: 11502325
 * Date: 29/09/2017
 * Time: 10:52
 */

namespace model;

use interfaces\problemInterface;

class PDOProblemRepository implements problemInterface
{
    private $pdo = null;

    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function getProblemByLocationId($location_id)
    {
        try
        {
            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $statement = $this->pdo->prepare('SELECT *  FROM problems WHERE location_id = ?');
            $statement->bindParam(1, $location_id, \PDO::PARAM_INT);
            $statement->setFetchMode(\PDO::FETCH_ASSOC);
            $statement->execute();
            $return = $statement->fetchAll();
            $output = [];
            if (count($return) > 0) {
                foreach ($return as $row) {
                    array_push($output, new Problem($row["id"], $row["location_id"], $row["date"], $row["description"], $row["status"],
                        $row["scheduled_date"], $row["picture"]));
                }
                header(http_response_code(200));
            } else{
                header(http_response_code(404));
            }
            return $output;
        }
        catch (\PDOException $e)
        {
            header(http_response_code(400));
        }

        return null;
    }

    public function getProblembyId($problem_id)
    {
        try
        {
            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $statement = $this->pdo->prepare('SELECT *  FROM problems WHERE id = ?');
            $statement->bindParam(1, $problem_id, \PDO::PARAM_INT);
            $statement->execute();
            $return = $statement->fetchAll(\PDO::FETCH_ASSOC);
            if (count($return) > 0) {
                foreach ($return as $row) {
                    $output = new Problem($row['id'], $row['location_id'], $row['date'], $row['description'], $row['status'],
                        $row['scheduled_date'], $row['picture']);
                    header(http_response_code(200));
                    return $output;
                }
            } else{
                header(http_response_code(404));
            }
        }
        catch (\PDOException $e)
        {
            header(http_response_code(400));
        }
        return null;
    }


    public function getProblemsByScheduledDate($dateString)
    {
        try
        {
            $date = date_create($dateString);
            if($date === false){
              throw new \InvalidArgumentException($dateString . ' is not a valid date');
            };
            $formattedDate = date_format($date, "Y-m-d");

            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $statement = $this->pdo->prepare('SELECT *  FROM problems WHERE scheduled_date = ?');
            $statement->bindParam(1, $formattedDate, \PDO::PARAM_INT);
            $statement->setFetchMode(\PDO::FETCH_ASSOC);
            $statement->execute();
            $return = $statement->fetchAll();
            $output = [];
            if (count($return) > 0) {
                foreach ($return as $row) {
                    array_push($output, new Problem($row["id"], $row["location_id"], $row["date"], $row["description"], $row["status"],
                        $row["scheduled_date"], $row["picture"]));
                }
                header(http_response_code(200));

            } else{
                header(http_response_code(404));
            }
            return $output;
        }
        catch (\PDOException $e)
        {
            header(http_response_code(400));
        }
        catch (\InvalidArgumentException $e){
            header(http_response_code(400));
        }
        return null;
    }

    public function addProblem(Problem $problem)
    {
        try
        {
            $date = date_create($problem->getDate());
            if($date === false){
                throw new \InvalidArgumentException($problem->getDate() . ' is not a valid date');
            };
            $formattedDate = date_format($date, "Y-m-d");
            $loc_id = $problem->getLocationId();
            $datum = $problem->getDate();
            $desc = $problem->getDescription();
            $status = $problem->getStatus();
            $planned = $problem->getScheduledDate();
            $picture = $problem->getPicture();
            $problem->getPicture();
            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $statement = $this->pdo->prepare("INSERT INTO `problems`(`id`, `location_id`, `date`, `description`, `status`, `scheduled_date`, `picture`)
            VALUES(NULL , :location_id, :datum, :description,:status,:scheduled_date,:picture);");
            $statement->bindParam(':location_id',  $loc_id, \PDO::PARAM_INT);
            $statement->bindParam(':datum',  $formattedDate, \PDO::PARAM_STR);
            $statement->bindParam(':description',  $desc, \PDO::PARAM_STR);
            $statement->bindParam(':status', $status, \PDO::PARAM_INT);
            $statement->bindParam(':scheduled_date', $planned, \PDO::PARAM_STR);
            $statement->bindParam(':picture', $picture, \PDO::PARAM_STR);
            $statement->execute();
            header(http_response_code(200));
            $id = $this->pdo->lastInsertId();
            $problem->setId($id);
            return $problem;
        }
        catch (\PDOException $e)
        {
            header(http_response_code(400));
        }
        return null;
    }

    public function updateProblem(Problem $problem)
    {
        try
        {
            $id = $problem->getId();
            $loc_id = $problem->getLocationId();
            $datum = $problem->getDate();
            $desc = $problem->getDescription();
            $status = $problem->getStatus();
            $planned = $problem->getScheduledDate();
            $picture = $problem->getPicture();

            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $statement = $this->pdo->prepare("UPDATE `problems` SET 
                `location_id` = :location_id, 
                `date` = :datum, 
                `description` = :description, 
                `status` = :status,
                `scheduled_date` = :scheduled_date, 
                `picture` = :picture
                WHERE id = :id;");
            $statement->bindParam(':location_id',  $loc_id, \PDO::PARAM_INT);
            $statement->bindParam(':datum',  $datum, \PDO::PARAM_STR);
            $statement->bindParam(':description',  $desc, \PDO::PARAM_STR);
            $statement->bindParam(':status', $status, \PDO::PARAM_INT);
            $statement->bindParam(':scheduled_date', $planned, \PDO::PARAM_STR);
            $statement->bindParam(':picture', $picture, \PDO::PARAM_STR);
            $statement->bindParam(':id',  $id, \PDO::PARAM_INT);
            $statement->execute();
            header(http_response_code(200));
            return $problem;
        }
        catch (\PDOException $e)
        {
            header(http_response_code(400));
        }
        return null;
    }


    public function getAllProblems()
    {
        try
        {
            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $statement = $this->pdo->prepare('SELECT *  FROM problems');
            $statement->setFetchMode(\PDO::FETCH_ASSOC);
            $statement->execute();
            $return = $statement->fetchAll();
            $output = [];
            if ($return !== null) {
                foreach ($return as $row) {
                    array_push($output, new Problem($row["id"], $row["location_id"], $row["date"], $row["description"], $row["status"],
                        $row["scheduled_date"], $row["picture"]));
                }
            }
            header(http_response_code(200));
            return $output;
        }
        catch (\PDOException $e)
        {
            header(http_response_code(400));
        }
        return null;
    }
}
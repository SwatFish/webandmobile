<?php

namespace model;

use interfaces\technicianInterface;

class PDOTechnicianRepository implements technicianInterface
{
    private $pdo = null;

    public function __construct(\PDO $pdo) {
        $this->pdo = $pdo;
    }

    public function getTechnicianById($id)
    {
        try {
            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $statement = $this->pdo->prepare('SELECT * FROM technicians WHERE id = ?');
            $statement->bindParam(1, $id, \PDO::PARAM_INT);
            $statement->execute();
            $return = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $output = [];
            if (count($return) > 0) {
                foreach ($return as $technician) {
                    $output = new Technician($technician['id'], $technician['first_name'], $technician['last_name']);
                    header(http_response_code(200));
                }
            } else{
                header(http_response_code(404));
            }
            return $output;

        } catch (\PDOException $e) {
            header(http_response_code(400));
        }
        return null;
    }

    public function addTechnician(Technician $technician)
    {
        try {
            $first_name = $technician->getFirstName();
            $last_name = $technician->getLastName();

            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $statement = $this->pdo->prepare("INSERT INTO `technicians`(`first_name`, `last_name`)VALUES(:first_name, :last_name);");
            $statement->bindParam(':first_name',  $first_name, \PDO::PARAM_STR);
            $statement->bindParam(':last_name',  $last_name, \PDO::PARAM_STR);
            $statement->execute();
            header(http_response_code(200));
            $id = $this->pdo->lastInsertId();
            $technician->setId($id);

            return $technician;
        }
        catch (\PDOException $e)
        {
            header(http_response_code(400));
        }
        return null;
    }

    public function getAllTechnicians() {
        try
        {
            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $statement = $this->pdo->prepare('SELECT * FROM technicians');
            $statement->setFetchMode(\PDO::FETCH_ASSOC);
            $statement->execute();
            $output = [];
            $return = $statement->fetchAll();

            if ($return !== null) {
                foreach ($return as $row) {
                    array_push($output, new Technician($row["id"], $row["first_name"], $row["last_name"]));
                }
            }

            if (count($output) > 0) {
                header(http_response_code(200));
                return $output;
            } else {
                return null;
            }
        }
        catch (\PDOException $e)
        {
            header(http_response_code(400));
        }
        $this->pdo = null;
        return null;
    }

    public function updateTechnician(Technician $technician)
    {
        try {
            $id = $technician->getId();
            $first_name = $technician->getFirstName();
            $last_name = $technician->getLastName();

            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $statement = $this->pdo->prepare("UPDATE `technicians` SET 
            `first_name` = :first_name, `last_name` = :last_name
             WHERE `id` = :id;");
            $statement->bindParam(':first_name', $first_name, \PDO::PARAM_STR);
            $statement->bindParam(':last_name', $last_name, \PDO::PARAM_STR);
            $statement->bindParam(':id',  $id, \PDO::PARAM_INT);
            $statement->execute();
            header(http_response_code(200));
            return $technician;
        }
        catch (\PDOException $e)
        {
            header(http_response_code(400));
        }
        return null;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: 11502325
 * Date: 29/09/2017
 * Time: 10:52
 */

namespace model;

use interfaces\locationInterface;

 class PDOLocationRepository  implements locationInterface
{
    private $pdo = null;

    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function getAllLocations(){
        try
        {
            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $statement = $this->pdo->prepare('SELECT * FROM locations');
            $statement->setFetchMode(\PDO::FETCH_ASSOC);
            $statement->execute();
            $output = [];
            $return = $statement->fetchAll();

            if ($return !== null) {
                foreach ($return as $row) {
                    array_push($output, new Location($row["id"], $row["name"], $row["place"]));
                }
            }

            if (count($output) > 0) {
                header(http_response_code(200));
                return $output;
            } else {
                return null;
            }
        }
        catch (\PDOException $e)
        {
            header(http_response_code(400));
        }
        return null;
    }

     public function addLocation(Location $location)
     {
         try
         {
             $name = $location->getName();
             $place = $location->getPlace();
             $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
             $statement = $this->pdo->prepare("INSERT INTO `locations`(`id`, `name`, `place`)VALUES(NULL , :naam, :place);");
             $statement->bindParam(':naam',  $name, \PDO::PARAM_STR);
             $statement->bindParam(':place',  $place, \PDO::PARAM_STR);
             $statement->execute();
             header(http_response_code(201));
             $id = $this->pdo->lastInsertId();
             $location->setId($id);

             return $location;
         }
         catch (\PDOException $e)
         {
             header(http_response_code(400));
         }
         return null;
     }

     public function updateLocation(location $location)
     {
         try {
             $id = $location->getId();
             $place = $location->getPlace();
             $name = $location->getName();

             $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
             $statement = $this->pdo->prepare("UPDATE `locations` SET 
            `name` = :naam, `place` = :place
             WHERE `id` = :id;");
             $statement->bindParam(':naam', $name, \PDO::PARAM_STR);
             $statement->bindParam(':place', $place, \PDO::PARAM_STR);
             $statement->bindParam(':id',  $id, \PDO::PARAM_INT);
             $statement->execute();
             header(http_response_code(200));
             return $location;
         }
         catch (\PDOException $e)
         {
             header(http_response_code(400));
         }
         return null;
     }
 }
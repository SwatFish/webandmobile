<?php
/**
 * Created by PhpStorm.
 * User: 11502325
 * Date: 29/09/2017
 * Time: 10:54
 */

namespace model;
use interfaces\statusInterface;

class PDOStatusRepository implements statusInterface
{
    private $pdo = null;

    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function getStatusByLocationId($location_id)
    {
        try
        {
            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $statement = $this->pdo->prepare('SELECT *  FROM statuses WHERE location_id = ?');
            $statement->bindParam(1, $location_id, \PDO::PARAM_INT);
            $statement->setFetchMode(\PDO::FETCH_ASSOC);
            $statement->execute();
            $return = $statement->fetchAll();
            $output = [];

            if (count($return) > 0) {
                foreach ($return as $row) {
                    array_push($output, new Status($row["id"],$row["location_id"], $row["status"], $row["date"]));
                }
                header(http_response_code(200));
            } else{
                header(http_response_code(404));
            }
            return $output;
        }
        catch (\PDOException $e)
        {
            header(http_response_code(400));
        }
        return null;
    }

    public function createStatus(Status $status)
    {
        try
        {
            $date = date_create($status->getDate());
            if($date === false){
                throw new \InvalidArgumentException($status->getDate() . ' is not a valid date');
            };
            $formattedDate = date_format($date, "Y-m-d");
            $loc_id = $status->getLocationId();
            $state = $status->getStatus();

            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $statement = $this->pdo->prepare("INSERT INTO `statuses`(`id`, `location_id`, `status`, `date`)
            VALUES(NULL , :location_id, :status, :datum);");
            $statement->bindParam(':location_id',  $loc_id, \PDO::PARAM_INT);
            $statement->bindParam(':status', $state, \PDO::PARAM_INT);
            $statement->bindParam(':datum',  $formattedDate, \PDO::PARAM_STR);
            $statement->execute();
            header(http_response_code(200));
            $id = $this->pdo->lastInsertId();
            $status->setId($id);
            return $status;
        }
        catch (\PDOException $e)
        {
            header(http_response_code(400));
        }
        catch (\InvalidArgumentException $e){
            header(http_response_code(400));
        }
        return null;
    }
}
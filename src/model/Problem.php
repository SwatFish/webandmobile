<?php

namespace model;


class Problem implements \JsonSerializable
{
    private $id;
    private $location_id;
    private $date;
    private $description;
    private $status;
    private $scheduled_date;
    private $picture;

    /**
     * Problem constructor.
     * @param $id
     * @param $location_id
     * @param $date
     * @param $description
     * @param $status
     * @param $scheduled_date
     * @param $picture
     */
    public function __construct($id, $location_id, $date, $description, $status, $scheduled_date, $picture)
    {
        $this->setId($id);
        $this->setLocationId($location_id);
        $this->setDate($date);
        $this->setDescription($description);
        $this->setStatus($status);
        $this->setScheduledDate($scheduled_date);
        $this->setPicture($picture);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLocationId()
    {
        return $this->location_id;
    }

    /**
     * @param mixed $location_id
     */
    public function setLocationId($location_id)
    {
        $this->location_id = $location_id;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getScheduledDate()
    {
        return $this->scheduled_date;
    }

    /**
     * @param mixed $scheduled_date
     */
    public function setScheduledDate($scheduled_date)
    {
        $this->scheduled_date = $scheduled_date;
    }

    /**
     * @return mixed
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param mixed $picture
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
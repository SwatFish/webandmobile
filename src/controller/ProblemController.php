<?php

namespace controller;


use interfaces\problemInterface;
use Model\Problem;
use view\JSonView;

class ProblemController
{
    private  $repository;
    private $problemView;

    public function __construct(problemInterface $repository)
    {
        $this->repository = $repository;
        $this->problemView = new JSonView();
    }

    public function getProblemByLocationId($location_id)
    {
        $results = $this->repository->getProblemByLocationId($location_id);
        $this->problemView->showData($results);
    }

    public function getProblemById($id)
    {
        $results = [];
        $problem =  $this->repository->getProblembyId($id);
        array_push($results,$problem);
        $this->problemView->showData($results);
    }

    public function getProblemsByScheduledDate($date)
    {
        $results = $this->repository->getProblemsByScheduledDate($date);
        $this->problemView->showData($results);
    }

    public function createProblem(Problem $problem)
    {
        $results = [];
        $problem =  $this->repository->addProblem($problem);
        array_push($results,$problem);
        $this->problemView->showData($results);
    }

    public function updateProblem(Problem $problem){
        $results = [];
        $problem =  $this->repository->updateProblem($problem);
        array_push($results,$problem);
        $this->problemView->showData($results);
    }

    public function getAllProblems()
    {
        $results = $this->repository->getAllProblems();
        $this->problemView->showData($results);
    }


}
<?php

namespace controller;
use interfaces\locationInterface;
use model\PDOLocationRepository;
use model\Location;
use view\JSonView;

class LocationController
{
    private $repository;
    private $locationView;

    public function __construct(PDOLocationRepository $repository)
    {
        $this->repository = $repository;
        $this->locationView = new JSonView();
    }

    public function handleGetLocations() {
        $results = $this->repository->getAllLocations();
        $this->locationView->showData($results);
    }

    public function createLocation(Location $location) {
        $results = [];
        $location =  $this->repository->addLocation($location);
        array_push($results,$location);
        $this->locationView->showData($results);
    }

    public function updateLocation(Location $location) {
        $results = [];
        $location =  $this->repository->updateLocation($location);
        array_push($results,$location);
        $this->locationView->showData($results);
    }


}
<?php

namespace controller;

use interfaces\technicianInterface;
use model\Technician;
use view\JSonView;

class TechnicianController
{
    private $repository;
    private $technicianView;

    public function __construct(technicianInterface $repository) {
        $this->repository = $repository;
        $this->technicianView = new JsonView();
    }

    public function getTechnicianById($id) {
        $results = [];
        $technician =  $this->repository->getTechnicianById($id);
        array_push($results,$technician);
        $this->technicianView->showData($results);
    }

    public function getAllTechnicians() {
        $results = $this->repository->getAllTechnicians();
        $this->technicianView->showData($results);
    }

    public function createTechnician(Technician $technician)
    {
        $results = [];
        $technician=  $this->repository->addTechnician($technician);
        array_push($results,$technician);
        $this->technicianView->showData($results);
    }

    public function updateTechnician(Technician $technician) {
        $results = [];
        $technician =  $this->repository->updateTechnician($technician);
        array_push($results,$technician);
        $this->technicianView->showData($results);
    }
}
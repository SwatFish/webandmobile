<?php

namespace controller;
use interfaces\statusInterface;
use model\Status;
use view\JSonView;

class StatusController
{
    private $repository;
    private $statusView;

    public function __construct(statusInterface $repository)
    {
        $this->repository = $repository;
        $this->statusView = new JSonView();
    }

    public function getStatusesByLocationId($location_id)
    {
        $results = $this->repository->getStatusByLocationId($location_id);
        $this->statusView->showData($results);
    }

    public function createStatus(Status $status)
    {
        $results = [];
        $status =  $this->repository->createStatus($status);
        array_push($results,$status);
        $this->statusView->showData($results);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 30/09/17
 * Time: 19:12
 */



$config = include 'config.php';
$user = $config['username'];
$password = $config['password'];
$dbname = $config['dbname'];

try {
    $pdo = new PDO( "mysql:host=localhost;dbname=".$dbname,
        $user, $password );
    $pdo->setAttribute( PDO::ATTR_ERRMODE,
        PDO::ERRMODE_EXCEPTION );
   return $pdo;
} catch ( PDOException $e ) {
    print 'Exception!: ' . $e->getMessage();
}
<?php
/**
 * Created by PhpStorm.
 * User: joachim
 * Date: 16/10/2017
 * Time: 14:26
 */

//require_once '../../../vendor/autoload.php';

use model\Status;
use model\PDOStatusRepository;


class PDOStatusRepositoryTest extends PHPUnit\Framework\TestCase
{
    private $pdoRepository;

    public function setUp()
    {
        $this->mockPDO = $this->getMockBuilder('PDO')
            ->disableOriginalConstructor()
            ->getMock();
        $this->mockPDOStatement =
            $this->getMockBuilder('PDOStatement')
                ->disableOriginalConstructor()
                ->getMock();
        $this->pdoRepository = new PDOStatusRepository($this->mockPDO);
    }

    public function testFindStatusByLocationId_IfExists_StatusObject() {
        $status1 = new Status(1, 1, 1,"1997-02-17");
        $status2 = new Status(2, 2,2, "1997-02-17");
        $allStatusses = Array($status1,$status2);
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue(
                [
                    [ 'id' => $status1->getId(),
                        'location_id' => $status1->getLocationId(),
                        'status' => $status1->getStatus(),
                            'date' => $status1->getDate()
                    ],
                    [ 'id' => $status2->getId(),
                        'location_id' => $status2->getLocationId(),
                        'status' => $status2->getStatus(),
                        'date' => $status2->getDate()
                    ]
                ]));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));

        $actualStatus = $this->pdoRepository->getStatusByLocationId($status1->getLocationId());

        $this->assertEquals($allStatusses, $actualStatus);
    }

    public function testFindStatusByLocationId_IfDoesNotExist_ReturnsEmptyArray() {
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue([]));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));

        $actualStatus = $this->pdoRepository->getStatusByLocationId(10000);
        $this->assertEquals($actualStatus, [] );
    }

    public function testAddStatus_StatusObjectIsCorrect_ReturnsStatusObject()
    {
        $status = new Status(1,2,3,"1997-02-17");
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('lastInsertId')
            ->will($this->returnValue($status));

        $actualStatus = $this->pdoRepository->createStatus($status);

        $this->assertEquals($status, $actualStatus);
    }


    public function tearDown()
    {
        $this->mockPDO = null;
        $this->mockPDOStatement = null;
        $this->pdoRepository = null;
    }

}
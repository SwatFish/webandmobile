<?php
/**
 * Created by PhpStorm.
 * User: joachim
 * Date: 16/10/2017
 * Time: 13:55
 */

//require_once '../../../vendor/autoload.php';

use model\Location;
use model\PDOLocationRepository;

class PDOLocationRepositoryTest extends PHPUnit\Framework\TestCase
{
    private $pdoRepository;

    public function setUp() {
        $this->mockPDO = $this->getMockBuilder('PDO')
            ->disableOriginalConstructor()
            ->getMock();
        $this->mockPDOStatement =
            $this->getMockBuilder('PDOStatement')
                ->disableOriginalConstructor()
                ->getMock();
        $this->pdoRepository = new PDOLocationRepository($this->mockPDO);
    }

    public function testGetAllLocations_IfExists_AllLocations()
    {
        $location1 = new Location(1, 'GENT',"Gent");
        $location2 = new Location(2, "PXL","Hasselt");
        $allLocations= Array($location1, $location2);
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue(
                [
                    [ 'id' => $location1->getId(),
                        'name' => $location1->getName(),
                            'place' => $location1->getPlace()
                    ],
                    [ 'id' => $location2->getId(),
                        'name' => $location2->getName(),
                            'place' => $location2->getPlace()
                    ]
                ]));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $actualLocations = $this->pdoRepository->getAllLocations();

        $this->assertEquals($allLocations, $actualLocations);
    }

    public function testGetAllLocations_IfNoLocations_ThenNull()
    {
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue([]));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $actualLocation = $this->pdoRepository->getAllLocations();
        $this->assertEquals($actualLocation, null);
    }

    public function testGetAllLocations_exceptionThrown_WhenNull()
    {
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $actualLocation = $this->pdoRepository->getAllLocations();
        $this->assertEquals($actualLocation, null);
    }

    public function testAddLocation_LocationObjectIsCorrect_ReturnsLocationObject()
    {
        $location = new Location(1,"Jojos Place", "Halen");
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('lastInsertId')
            ->will($this->returnValue($location));
        $actualLocation = $this->pdoRepository->addLocation($location);
        $this->assertEquals($location, $actualLocation);
    }

    public function tearDown()
    {
        $this->mockPDO = null;
        $this->mockPDOStatement = null;
        $this->pdoRepository = null;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: joachim
 * Date: 16/10/2017
 * Time: 14:27
 */

//require_once '../../../vendor/autoload.php';

use model\Technician;
use model\PDOTechnicianRepository;


class PDOTechnicianRepositoryTest extends PHPUnit\Framework\TestCase
{
    private $pdoRepository;

    public function setUp()
    {
        $this->mockPDO = $this->getMockBuilder('PDO')
            ->disableOriginalConstructor()
            ->getMock();
        $this->mockPDOStatement =
            $this->getMockBuilder('PDOStatement')
                ->disableOriginalConstructor()
                ->getMock();
        $this->pdoRepository = new PDOTechnicianRepository($this->mockPDO);
    }

    public function testGetAllTechnicians_IfExists_AllTechnicians()
    {
        $technician1 = new Technician(1,"Andres","Belgy");
        $technician2 = new Technician(2, "Robbe","Kimpen");
        $allTechnicians= Array($technician1, $technician2);
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue(
                [
                    [ 'id' => $technician1->getId(),
                        'first_name' => $technician1->getFirstName(),
                        'last_name' => $technician1->getLastName()
                    ],
                    [ 'id' => $technician2->getId(),
                        'first_name' => $technician2->getFirstName(),
                        'last_name' => $technician2->getLastName()
                    ]
                ]));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $actualTechnicians = $this->pdoRepository->getAllTechnicians();

        $this->assertEquals($allTechnicians, $actualTechnicians);
    }

    public function testGetAllTechnicians_IfNoTechnicians_ThenNull()
    {
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue([]));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));

        $actualTechnician = $this->pdoRepository->getAllTechnicians();
        $this->assertEquals($actualTechnician, null);
    }

    public function testGetAllTechnicians_exceptionThrown_WhenNull()
    {
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $actualTechnician = $this->pdoRepository->getAllTechnicians();
        $this->assertEquals($actualTechnician, null);
    }

    public function testGetTechnicianById_Exists_TechnicianObject()
    {
        $technician = new Technician(1,"Andres","Belgy");

        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue(
                [
                    [ 'id' => $technician->getId(),
                        'first_name' => $technician->getFirstName(),
                        'last_name' => $technician->getLastName(),
                    ]
                ]));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));

        $actualTechnician = $this->pdoRepository->getTechnicianById($technician->getId());
        $this->assertEquals($technician, $actualTechnician);
    }

    public function testGetTechnicianById_IfDoesNotExist_ReturnsNull()
    {
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue([]));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));

        $actualTechnician = $this->pdoRepository->getTechnicianById(10000);
        $this->assertEquals($actualTechnician, null);
    }

    public function testAddTechnician_TechnicianObjectIsCorrect_ReturnsTechnicianObject()
    {
        $technician = new Technician(1,"Joachim","Zeelmaekers");
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('lastInsertId')
            ->will($this->returnValue($technician));
        $actualTechnician = $this->pdoRepository->addTechnician($technician);
        $this->assertEquals($technician, $actualTechnician);
    }

    public function tearDown()
    {
        $this->mockPDO = null;
        $this->mockPDOStatement = null;
        $this->pdoRepository = null;
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: joachim
 * Date: 16/10/2017
 * Time: 14:26
 */

//require_once '../../../vendor/autoload.php';

use model\Problem;
use model\PDOProblemRepository;

class PDOProblemRepositoryTest extends PHPUnit\Framework\TestCase
{
    private $pdoRepository;

    public function setUp()
    {
        $this->mockPDO = $this->getMockBuilder('PDO')
            ->disableOriginalConstructor()
            ->getMock();
        $this->mockPDOStatement =
            $this->getMockBuilder('PDOStatement')
                ->disableOriginalConstructor()
                ->getMock();
        $this->pdoRepository = new PDOProblemRepository($this->mockPDO);
    }

    public function testGetProblemByLocationId_IfExists_ProblemObjectArray()
    {
        $problem1 = new Problem(1, 1, "1997-02-17", "Test", 1, "1997-02-17", "");
        $problem2 = new Problem(2, 2, "1997-02-17", "Test2", 1, "1997-02-17", "");
        $allProblems = Array($problem1, $problem2);
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue(
                [
                    ['id' => $problem1->getId(),
                        'location_id' => $problem1->getLocationId(),
                        'date' => $problem1->getDate(),
                        'description' => $problem1->getDescription(),
                        'status' => $problem1->getStatus(),
                        'scheduled_date' => $problem1->getScheduledDate(),
                        'picture' => $problem1->getPicture()

                    ],
                    [
                        'id' => $problem2->getId(),
                        'location_id' => $problem2->getLocationId(),
                        'date' => $problem2->getDate(),
                        'description' => $problem2->getDescription(),
                        'status' => $problem2->getStatus(),
                        'scheduled_date' => $problem2->getScheduledDate(),
                        'picture' => $problem2->getPicture()
                    ]
                ]));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $actualProblems = $this->pdoRepository->getProblemByLocationId($problem1->getLocationId());
        $this->assertEquals($allProblems, $actualProblems);
    }

    public function testGetProblemByLocationId_IfDoesNotExist_ReturnsEmptyArray()
    {
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue([]));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $actualStatus = $this->pdoRepository->getProblemByLocationId(10000);
        $this->assertEquals($actualStatus, []);
    }

    public function testGetProblemById_Exists_ProblemObject()
    {
        $problem = new Problem(1, 1, "1997-02-17", "Test", 1, "1997-02-17", "");
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue(
                [
                    ['id' => $problem->getId(),
                        'location_id' => $problem->getLocationId(),
                        'date' => $problem->getDate(),
                        'description' => $problem->getDescription(),
                        'status' => $problem->getStatus(),
                        'scheduled_date' => $problem->getScheduledDate(),
                        'picture' => $problem->getPicture()
                    ],
                ]));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $actualProblem = $this->pdoRepository->getProblemById($problem->getId());
        $this->assertEquals($problem, $actualProblem);
    }

    public function testGetProblemById_IfDoesNotExist_ReturnsNull()
    {
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue([]));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $actualProblem = $this->pdoRepository->getProblemById(10000);
        $this->assertEquals($actualProblem, null);
    }

    public function testGetProblemByScheduledDate_IfExists_ProblemObjectArray()
    {
        $problem1 = new Problem(1, 1, "1997-02-17", "Test", 1, "1997-02-17", "");
        $problem2 = new Problem(2, 2, "1997-02-17", "Test2", 1, "1997-02-17", "");
        $allProblems = Array($problem1, $problem2);
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue(
                [
                    ['id' => $problem1->getId(),
                        'location_id' => $problem1->getLocationId(),
                        'date' => $problem1->getDate(),
                        'description' => $problem1->getDescription(),
                        'status' => $problem1->getStatus(),
                        'scheduled_date' => $problem1->getScheduledDate(),
                        'picture' => $problem1->getPicture()

                    ],
                    [
                        'id' => $problem2->getId(),
                        'location_id' => $problem2->getLocationId(),
                        'date' => $problem2->getDate(),
                        'description' => $problem2->getDescription(),
                        'status' => $problem2->getStatus(),
                        'scheduled_date' => $problem2->getScheduledDate(),
                        'picture' => $problem2->getPicture()
                    ]
                ]));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $actualProblems = $this->pdoRepository->getProblemsByScheduledDate('1997-02-17');
        $this->assertEquals($allProblems, $actualProblems);
    }

    public function testGetProblemByScheduledDate_IfDoesNotExist_ReturnsEmptyArray()
    {
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('fetchAll')
            ->will($this->returnValue([]));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $actualStatus = $this->pdoRepository->getProblemsByScheduledDate('1900-1-1');
        $this->assertEquals($actualStatus, []);
    }

    public function testAddProblem_ProblemObjectIsCorrect_ReturnsProblemObject()
    {
        $problem = new Problem(1, 5, "1997-02-17", "Test", 3, "1998-02-17", null);
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('lastInsertId')
            ->will($this->returnValue($problem));
        $actualProblem = $this->pdoRepository->addProblem($problem);
        $this->assertEquals($problem, $actualProblem);
    }

    public function testUpdateProblem_IfProblemExist_ReturnsProblemObject()
    {
        $problem = new Problem(1, 5, "1997-02-17", "Test", 0, "1998-02-17", null);
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('bindParam');
        $this->mockPDOStatement->expects($this->atLeastOnce())
            ->method('execute');
        $this->mockPDO->expects($this->atLeastOnce())
            ->method('prepare')
            ->will($this->returnValue($this->mockPDOStatement));
        $actualProblem = $this->pdoRepository->updateProblem($problem);
        $this->assertEquals($problem, $actualProblem);
    }

    public function tearDown()
    {
        $this->mockPDO = null;
        $this->mockPDOStatement = null;
        $this->pdoRepository = null;
    }
}
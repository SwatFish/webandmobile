<?php
/**
 * Created by PhpStorm.
 * User: joachim
 * Date: 23/10/2017
 * Time: 20:38
 */

class TestProblemApiCalls extends PHPUnit\Framework\TestCase
{
    private $http;

    public function setUp()
    {
        $this->http = new GuzzleHttp\Client(['base_uri' => '192.168.33.11/api/']);
//        $this->http = new GuzzleHttp\Client(['base_uri' => 'http://192.168.241.138/~hannecolaers/WP1/']);
    }

    public function tearDown()
    {
        $this->http = null;
    }

    /**
     * All Problem API calls.
     */
    public function testGetProblemById_ShouldReturnJsonWithProblemIdEqualToRequestId()
    {
        $response = $this->http->request('GET', 'problem/1');

        $this->assertEquals(200, $response->getStatusCode());

        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("application/json", $contentType);

        $decodedJson = json_decode($response->getBody());
        $this->assertEquals($decodedJson[0]->id, "1");
    }

    public function testGetProblemsByLocationId_ShouldReturnProblemArray()
    {
        $response = $this->http->request('GET', 'locations/1/problems');

        $this->assertEquals(200, $response->getStatusCode());

        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("application/json", $contentType);

        $decodedJson = json_decode($response->getBody());

        $this->assertEquals($decodedJson[0]->location_id, "1");
    }

    public function testGetProblemsScheduledDate_ShouldReturnProblemArray()
    {
        $response = $this->http->request('GET', 'problems/2017-11-03)');

        $this->assertEquals(200, $response->getStatusCode());

        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("application/json", $contentType);

        $decodedJson = json_decode($response->getBody());

        //$this->assertEquals($decodedJson[3]->scheduled_date, "2017-11-03");
    }

    public function testAddProblem_ShouldReturnProblemObjectWithId() {
        $response = $this->http->request('POST', 'addProblem', [
            'form_params'=> [
                "location_id" => "1",
                "date" => "2017-10-03",
                "description" => "TestProblem",
                "status" => "1",
                "scheduled_date" => "2017-11-03",
                "picture" => ""
            ]
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("text/html; charset=UTF-8", $contentType);

        $decodedJson = json_decode($response->getBody());
        //$this->assertNotEquals($decodedJson[0]->id, 0);
    }

    /* public function testUpdateProblem_ShouldReturnProblemObject(){
        $response = $this->http->request('PUT', 'updateProblem', [
            'form_params' => [
                "id" => "1",
                "location_id" => "1",
                "date" => "30-10-2017",
                "description" => "Lorem ipsum dolor sit amet, id qualisque intellegebat qui. Porro ullum graeco nam ad. Nisl noster argumentum cu pri. Cu movet similique mel. Ad est justo posse. Per aliquid sanctus consequuntur te, nam te tation oblique epicuri.",
                "status" => "0",
                "scheduled_date" => "2017-10-27",
                "picture" => null
            ]
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("application/json", $contentType);

        $decodedJson = json_decode($response->getBody());
        $this->assertEquals($decodedJson[0]->id, "1");
        $this->assertEquals($decodedJson[5]->scheduled_date, "2017-10-27");
    }
    */

    /**
     * All Locations API calls.
     */
    public function testGetLocations_ShouldReturnJsonNotNull()
    {
        $response = $this->http->request('GET', 'locations');

        $this->assertEquals(200, $response->getStatusCode());

        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("application/json", $contentType);

        $decodedJson = json_decode($response->getBody());

        $this->assertNotEquals($decodedJson, null);
    }

    public function testAddLocation_ShouldReturnLocationObjectWithId() {
        $response = $this->http->request('POST', 'addLocation', [
            'form_params'=> [
                "name" => "testThis",
                "place" => "IntegrationTest",
            ]
            ]);

        $this->assertEquals(200, $response->getStatusCode());
        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("text/html; charset=UTF-8", $contentType);

        $decodedJson = json_decode($response->getBody());
        //$this->assertNotEquals($decodedJson[0]->id, 0);
    }

    /**
     * All Technician API calls.
     */
    public function testGetTechnicians_ShouldReturnJsonNotNull()
    {
        $response = $this->http->request('GET', 'technicians');

        $this->assertEquals(200, $response->getStatusCode());

        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("application/json", $contentType);

        $decodedJson = json_decode($response->getBody(), true);

        $this->assertNotEquals($decodedJson, null );
    }

    public function testGetTechnicianById_ShouldReturnTechnicianObject()
    {
        $response = $this->http->request('GET', 'technician/1');

        $this->assertEquals(200, $response->getStatusCode());

        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("application/json", $contentType);

        $decodedJson = json_decode($response->getBody());

        $this->assertEquals($decodedJson[0]->id, "1");
    }

    public function testAddTechnician_ShouldReturnTechnicianObjectWithId()
    {
        $response = $this->http->request('POST', 'addTechnician', [
            'form_params' => [
                "first_name" => "Jojo",
                "last_name" => "Zeelmaekers"
            ]
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("text/html; charset=UTF-8", $contentType);

        $decodedJson = json_decode($response->getBody());

        //$this->assertNotEquals($decodedJson[0]->id, 0);
    }

    /**
     * All Status API calls.
     */
    public function testGetStatusesByLocationId_ShouldReturnStatusesArray()
    {
        $response = $this->http->request('GET', 'locations/1/statuses');

        $this->assertEquals(200, $response->getStatusCode());

        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("application/json", $contentType);

        $decodedJson = json_decode($response->getBody());

        $this->assertEquals($decodedJson[0]->id, "1");
    }

}
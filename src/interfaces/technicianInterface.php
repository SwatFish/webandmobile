<?php

namespace interfaces;

use model\Technician;

interface technicianInterface
{
    public function getTechnicianById($id);
    public function getAllTechnicians();
    public function addTechnician(Technician $technician);
    public function updateTechnician(Technician $technician);
}
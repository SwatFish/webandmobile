<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/10/17
 * Time: 19:18
 */

namespace interfaces;


use model\Problem;

interface problemInterface
{
    public function getProblemByLocationId($location_id);
    public function getProblembyId($problem_id);
    public function addProblem(Problem $problem);
    public function updateProblem(Problem $problem);
    public function getProblemsByScheduledDate($technicianId);
    public function getAllProblems();
}
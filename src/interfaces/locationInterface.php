<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/10/17
 * Time: 14:13
 */

namespace interfaces;


use model\Location;

interface locationInterface
{
    public function getAllLocations();
    public function addLocation(Location $location);
    public function updateLocation(location $location);
}
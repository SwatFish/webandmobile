<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/10/17
 * Time: 17:51
 */

namespace interfaces;

use model\Status;


interface statusInterface
{
    public function getStatusByLocationId($location_id);
    public function createStatus(Status $status);
}
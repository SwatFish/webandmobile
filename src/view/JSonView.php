<?php
/**
 * Created by PhpStorm.
 * User: 11502325
 * Date: 29/09/2017
 * Time: 10:31
 */

namespace view;


class JSonView implements View
{

    public function showData(array $data)
    {
        header('Content-Type: application/json');
        echo json_encode($data, JSON_PRETTY_PRINT);
    }


}
<?php
/**
 * Created by PhpStorm.
 * User: 11502325
 * Date: 29/09/2017
 * Time: 10:31
 */

namespace view;


interface View
{
    public function showData(array $data);
}
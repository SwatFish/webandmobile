<?php

require_once "./vendor/autoload.php";

use controller\LocationController;
use controller\ProblemController;
use controller\StatusController;
use controller\TechnicianController;

use model\PDOLocationRepository;
use model\PDOProblemRepository;
use model\PDOStatusRepository;
use model\PDOTechnicianRepository;


$user = 'root';
$password = 'root';
$database = 'testdb';
$hostname = 'localhost';

return [
    'LocationController' => DI\object(LocationController::class)->constructor(DI\object(PDOLocationRepository::class)
        ->constructor(DI\object(PDO::class)
            ->constructor("mysql:host=$hostname;dbname=$database", $user, $password, null))),
     'ProblemController' => DI\object(ProblemController::class)->constructor(DI\object(PDOProblemRepository::class)
         ->constructor(DI\object(PDO::class)
             ->constructor("mysql:host=$hostname;dbname=$database",$user,$password,null))),
     'StatusController' => DI\object(StatusController::class)->constructor(DI\object(PDOStatusRepository::class)
         ->constructor(DI\object(PDO::class)
             ->constructor("mysql:host=$hostname;dbname=$database",$user,$password,null))),
     'TechnicianController' => DI\object(TechnicianController::class)->constructor(DI\object(PDOTechnicianRepository::class)
         ->constructor(DI\object(PDO::class)
             ->constructor("mysql:host=$hostname;dbname=$database",$user,$password,null))),
];
